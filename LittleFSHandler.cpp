#include <ArduinoJson.h>
#include "LittleFS.h"
#include "LittleFSHandler.h"
// Get: data objects
#include "shared_objects.h"
#include "timeStamp.h"

#define FILE_PATH "/measurements.csv"  // File path for CSV
#define CSV_HEADER_ROW "Index,MCU_Millis,NTP_Time,Luminosity,Blank,Transmittance,Absorbance,Adj_Factor"

// Initialize LittleFS
void initFS() {
    if (!LittleFS.begin()) {
        Serial.println("An error has occurred while mounting LittleFS");
    } else {
        Serial.println("LittleFS mounted successfully.");
    }
}

// Write file to LittleFS
void writeJsonFile(const char * path, JsonDocument& data){
    // Adapted from Pablo Cremades
    // https://gitlab.fcen.uncu.edu.ar/pcremades/fermenter-temperature-controller
    Serial.printf("Writing to file: %s\r\n", path);
    // Open the file for writing
    File file = LittleFS.open(path, "w");
    if (!file) {
        Serial.println("Failed to open file for writing.");
        return;
    }
    // Serialize the JSON document to the file
    if (serializeJson(data, file) == 0) {
        Serial.println("Failed to write to file.");
    } else {
        Serial.println("File saved successfully.");
    }
    // Close the file connection.
    file.close();
}

// Function to save blank data to a JSON file in LittleFS
void writeBlankValues(JsonDocument& readings) {
    Serial.print("Writing blank values to persistent storage: ");
    serializeJson(readings, Serial);
    Serial.println();

    // Open the file for writing
    File file = LittleFS.open("/blank.json", "w");
    if (!file) {
        Serial.println("Failed to open blanks file for writing");
        return;
    }

    // Serialize the JSON document to the file
    if (serializeJson(readings, file) == 0) {
        Serial.println("Failed to write to blanks file");
    } else {
        Serial.println("Blanks file saved successfully");
    }

    // Close the file
    file.close();
}

// Function to read the blank values from the saved JSON file
void readBlankValues(JsonDocument& blanks) {
    // Open the JSON file for reading
    File file = LittleFS.open("/blank.json", "r");
    if (!file) {
        Serial.println("Failed to open blanks file for reading");
        return;
    }

    // Parse the JSON data
    DeserializationError error = deserializeJson(blanks, file);
    if (error) {
        Serial.print("Failed to parse blanks file: ");
        Serial.println(error.c_str());
        return;
    }

    Serial.print("Read blank values from persistent storage: ");
    serializeJson(blanks, Serial);
    Serial.println();

    // Close the file
    file.close();
    // Serial.println("Done parsing blanks file.");
}


// Function to save blank data to a JSON file in LittleFS
void writeWiFiConfig(JsonDocument& wifi_creds) {
    Serial.print("Writing WiFi values to persistent storage: ");
    serializeJson(wifi_creds, Serial);
    Serial.println();

    // Open the file for writing
    File file = LittleFS.open("/wifi.json", "w");
    if (!file) {
        Serial.println("Failed to open config file for writing");
        return;
    }

    // Serialize the JSON document to the file
    if (serializeJson(wifi_creds, file) == 0) {
        Serial.println("Failed to write to file");
    } else {
        Serial.println("Config file saved successfully");
    }

    // Close the file
    file.close();
}

// Function to save blank data to a JSON file in LittleFS
void readWiFiConfig(JsonDocument& wifi_creds) {
    // Open the JSON file for reading
    File file = LittleFS.open("/wifi.json", "r");
    if (!file) {
        Serial.println("Failed to open wifi file for reading");
        return;
    }

    // Parse the JSON data
    DeserializationError error = deserializeJson(wifi_creds, file);
    if (error) {
        Serial.print("Failed to parse wifi file: ");
        Serial.println(error.c_str());
        return;
    }

    Serial.print("Read wifi values from persistent storage: ");
    serializeJson(wifi_creds, Serial);
    Serial.println();

    // Close the file
    file.close();
    // Serial.println("Done parsing wifi_creds file.");
}

void writeSensorConfig(JsonDocument& sensor_config){
    Serial.print("Writing sensor values to persistent storage: ");
    serializeJson(sensor_config, Serial);
    Serial.println();

    // Open the file for writing
    File file = LittleFS.open("/sensor.json", "w");
    if (!file) {
        Serial.println("Failed to open sensor config file for writing.");
        return;
    }

    // Serialize the JSON document to the file
    if (serializeJson(sensor_config, file) == 0) {
        Serial.println("Failed to write to sensor config file.");
    } else {
        Serial.println("Sensor config file saved successfully.");
    }

    // Close the file
    file.close();
}

void readSensorConfig(JsonDocument& sensor_config){
    // Open the JSON file for reading
    File file = LittleFS.open("/sensor.json", "r");
    if (!file) {
        Serial.println("Failed to open sensor file for reading");
        return;
    }

    // Parse the JSON data
    DeserializationError error = deserializeJson(sensor_config, file);
    if (error) {
        Serial.print("Failed to parse sensor config file: ");
        Serial.println(error.c_str());
        return;
    }

    Serial.print("Read sensor values from persistent storage: ");
    serializeJson(sensor_config, Serial);
    Serial.println();

    // Close the file
    file.close();
}

void writeCalibrationConfig(JsonDocument& calib_config){
    Serial.print("Writing calibration values to persistent storage: ");
    serializeJson(calib_config, Serial);
    Serial.println();

    // Open the file for writing
    File file = LittleFS.open("/calibration.json", "w");
    if (!file) {
        Serial.println("Failed to open sensor calibration file for writing.");
        return;
    }

    // Serialize the JSON document to the file
    if (serializeJson(calib_config, file) == 0) {
        Serial.println("Failed to write to sensor calibration config file.");
    } else {
        Serial.println("Calibration config file saved successfully.");
    }

    // Close the file
    file.close();
}

void readCalibrationConfig(JsonDocument& calib_config){
    // Open the JSON file for reading
    File file = LittleFS.open("/calibration.json", "r");
    if (!file) {
        Serial.println("Failed to open calibration file for reading");
        return;
    }

    // Parse the JSON data
    DeserializationError error = deserializeJson(calib_config, file);
    if (error) {
        Serial.print("Failed to parse calibration file: ");
        Serial.println(error.c_str());
        return;
    }

    Serial.print("Read calibration values from persistent storage: ");
    serializeJson(calib_config, Serial);
    Serial.println();

    // Close the file
    file.close();
}

bool recreateMeasurementFile() {
  Serial.println("Clearing measurements.");
  // Open file in write mode (overwrite).
  File file = LittleFS.open(FILE_PATH, "w");
  if (!file) {
    Serial.println("Failed to create file.");
    return false;
  }

  // Write the header row for the CSV
  file.println(CSV_HEADER_ROW);
  file.close();

  Serial.println("File recreated with headers.");
  return true;
}


// Count of measurements in CSV file.
int savedCount = -1;

// Counter.
int countSavedMeasurements(){
  // Open file in read mode.
  File readable_file = LittleFS.open(FILE_PATH, "r");
  if (!readable_file) {
    Serial.println("Failed to open file for counting lines.");
    // Try recreating the file.
    if(!recreateMeasurementFile()){
        return -404;
    }
  }

  // Count lines by reading through the file
  int newLineCount = 0;  // Initialize line counter
  while (readable_file.available()) {
    String line = readable_file.readStringUntil('\n');
    if (line.length() > 0) {
      newLineCount++;
    }
  }

  // Close the file currently in read mode.
  readable_file.close();

  return newLineCount-1;
}


void appendMeasurementToFile() {
  Serial.println("Saving measurement.");

  // Get measurement count.
  savedCount = countSavedMeasurements();
  if (savedCount == -404){
    Serial.println("Error: file not available for saving a measurement.");
    return;
  }

  // Re-open file in append mode.
  File writable_file = LittleFS.open(FILE_PATH, "a");

  // Retrieve the value to append from the readings object
  if (!readings.containsKey("vis_abs")) {
    Serial.println("Error: 'vis_abs' not available in readings.");
    return;
  }

  // Get measurements.
  float visLum = readings["vis_lum"].as<float>();
  float visLumBlank = blanks["vis_lum"].as<float>();
  float visTrans = readings["vis_trans"].as<float>();
  float visAbs = readings["vis_abs"].as<float>();
  double visAbsAdj = calib_config["factor"].as<double>();

  // Get time stamp
  String timeStamp = getTimeStamp();

  // Append the new index and reading to the file.
  // "Index,MCU_Millis,NTP_Time,Blank,Transmittance,Absorbance,Adj_Factor"
  writable_file.printf("%d", savedCount+1);         // Index
  writable_file.printf(",%d", millis());            // MCU_Millis
  writable_file.printf(",%s", timeStamp.c_str());   // NTP_Time
  writable_file.printf(",%.2f", visLum);            // Luminosity
  writable_file.printf(",%.2f", visLumBlank);       // Blank
  writable_file.printf(",%.2f", visTrans);          // Transmittance
  writable_file.printf(",%.2f", visAbs);            // Absorbance
  writable_file.printf(",%.2f\n", visAbsAdj);       // Adj_Factor
  writable_file.close();

  Serial.printf("Appended: Index=%d, Reading=%.2f\n", savedCount+1, visAbs);

  // Update measurement count.
  savedCount = countSavedMeasurements();
}


void setup_defaults(){
  // Network defaults.
  readWiFiConfig(wifi_creds);
  // Blank defaults.
  readBlankValues(blanks);

  // Measurement line count.
  savedCount = countSavedMeasurements();

  // Sensor defaults.
  readSensorConfig(sensor_config);
  if(sensor_config.isNull()){
    sensor_config["gain"] = TSL2591_GAIN_DEFAULT;
    sensor_config["time"] = TSL2591_INTEGRATIONTIME_DEFAULT;
    writeSensorConfig(sensor_config);
  }

  // Calibration defaults.
  readCalibrationConfig(calib_config);
  if(calib_config.isNull()){
    calib_config["factor"] = 1.0;
    writeCalibrationConfig(calib_config);
  }
}
