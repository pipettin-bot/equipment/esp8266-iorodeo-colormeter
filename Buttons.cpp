////// Boton con librería OneButton ///////////
#include <OneButton.h>

#include "Buttons.h"

#include "Colormeter.h"
#include "OLED.h"
#include "sleep.h"
#include "LittleFSHandler.h"

// Track the time of the last button interaction
unsigned long lastButtonClickTime = 0;

// Timeout period in milliseconds (2 minutes)
const unsigned long DISPLAY_TIMEOUT = 120000;

void note_click(){
  lastButtonClickTime = millis();  // Update last click time
  // Turn on the display if it was off.
  if(!display_state){
    display_on();
  }
}

void checkDisplayTimeout() {
  if (millis() - lastButtonClickTime > DISPLAY_TIMEOUT & display_state) {
    display_off();
  }
}

#ifdef ESP8266

#define BUTTON_PIN D3
// Falta agregar toda la lógica del ESP8266 acá.

#elif defined(ESP32)
#define BUTTON_PIN_LEFT 0
#define BUTTON_PIN 47
#define BUTTON_PIN_RIGHT 48

// Configure left button.
OneButton btn_left = OneButton(BUTTON_PIN_LEFT, true, true);
// Handler function for a single left click.
static void handleLeftClick(){
  // Consider current state to avoid skipping the first screen after wrapping around.
  if(display_state){
    // Note the click before toggling if the display is on, avoiding turning it on on finall "off" screen.
    note_click();
    display_next();
  } else {
    // Note the click after toggling if the display was on, avoiding skipping the first screnn.
    display_next();
    note_click();
  }

}
// Handler function for a long left click.
static void handleLongLeftClick(){
  display_off(); // TODO: Replace with a "bye bye" screen.
  delay(1000);
  enterSleepMode(BUTTON_PIN_LEFT);
}

// Configure right button.
OneButton btn_right = OneButton(BUTTON_PIN_RIGHT, true, true);
// OneButton btn_right;
// Handler function for a single right click.
static void handleRightClick(){
  // Skip if display off.
  if (!display_state){
    note_click();
    return;
  }
  // Save a data point.
  Serial.println("Right click.");
  appendMeasurementToFile();
  // Display feedback message.
  draw_measurement(savedCount);
  // Show message for some time.
  delay(500);
  note_click();
}
// Handler function for a long right click.
static void handleLongRightClick(){
  // Skip if display off.
  if (!display_state){
    note_click();
    return;
  }
  // Clear data points.
  Serial.println("Right long click.");
  recreateMeasurementFile();
  // Reset measurement count (should be 0).
  savedCount = countSavedMeasurements();
  note_click();
}

#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

// Configure main button.
OneButton btn = OneButton(
  BUTTON_PIN, // Input pin for the button
  true,       // Button is active LOW
  true        // Enable internal pull-up resistor
);
// Handler function for a single click:
static void handleClick(){
  // Skip if display off.
  if (!display_state){
    note_click();
    return;
  }
  // Update blank values from current readings.
  updateBlanks();
  draw_blank();
  // Show message for some time.
  delay(500);
  note_click();
}
static void handleLongClick(){
  // Para que lo usaríamos?
  // Skip if display off.
  if (!display_state){
    note_click();
    return;
  }
  note_click();
}

void setup_buttons(){

  // Click simple
  btn.attachClick(handleClick);
  // Click largo
  btn.attachLongPressStart(handleLongClick);

  #if defined(ESP32)
  // Click izquierdo simple.
  btn_left.attachClick(handleLeftClick);
  // Click izquierdo largo.
  btn_left.attachLongPressStart(handleLongLeftClick);
  // Click derecho simple.
  btn_right.attachClick(handleRightClick);
  // Click derecho largo.
  btn_right.attachLongPressStart(handleLongRightClick);
  #endif
}

void tick_buttons(){
  btn.tick();

  #if defined(ESP32)
  btn_left.tick();
  btn_right.tick();
  checkDisplayTimeout();  // Check if the display should be turned off
  #endif
}
