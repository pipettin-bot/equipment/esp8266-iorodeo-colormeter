#include <Arduino.h>
#include <ArduinoJson.h>  // Parse JSON messsages from the serial interface.

// Main header
#include "json_rpc.h"
// Utils
#include "shared_objects.h"
#include "Colormeter.h"
#include "WiFiSetup.h"

float variable1 = 0.0;
float variable2 = 0.0;

JsonDocument make_state(void){
  // Create a temporary buffer for JSON serialization.
  JsonDocument tempDoc;
  
  // Copy 'readings' to 'tempDoc' to avoid modifying the original.
  tempDoc = readings;

  // Add gain and time information.
  tempDoc["current_gain"] = sensor_config["gain"];
  tempDoc["current_time"] = sensor_config["time"];

  // Add calibration factor.
  tempDoc["adj_factor"] = calib_config["factor"];

  return tempDoc;
}

// Deserialize a JSON document with ArduinoJson.
// https://arduinojson.org/v6/example/parser/
void parse_json_from_serial() {

  // Check if the computer is transmitting.
  while(Serial.available()) {

    // Skip newline characters.
    // Peek on the next byte from the Serial interface,
    // and skip it if it is a newline or carriage return.
    int next_byte = Serial.peek();

    // Newline '\n' has code 10, carriage-return has code 13.
    if(next_byte == 10 || next_byte == 13){
      // Get and remove the NL/CR byte from the serial buffer.
      Serial.read();

      // Notes:
      // For comparisons, a character is enclosed in single quotes ('\n') not double quotes.
      // https://www.cs.cmu.edu/~pattis/15-1XX/common/handouts/ascii.html
      // https://forum.arduino.cc/t/recognizing-new-line-character/619160/2
      // https://forum.arduino.cc/t/how-to-check-cr-in-the-char-from-serial-read-function/180110/4

    // Parse JSON and call methods.
    } else {
      // Allocate the JSON document for the incoming message.
      JsonDocument doc;

      // Read the JSON document from the "link" serial port.
      // Note: JSON reading terminates when a valid JSON is received, not considering line breaks.
      // Adding them to the message will result in incomplete/empty errors from deserializeJson.
      DeserializationError err = deserializeJson(doc, Serial);

      // Check if parsing went well.
      if (err == DeserializationError::Ok){

        // Allocate the JSON document for the response.
        JsonDocument response_doc;
        // Insert the ID or a default one if it is missing.
        bool has_id = doc.containsKey("id"); // true
        if(!has_id){
          response_doc["id"] = (int)-1;
        } else {
          response_doc["id"] = doc["id"];
        }

        // Check if received message is "getReadings"
        if (doc.containsKey("method")) {
          String method = doc["method"];

          if(method == "example"){
            // The following document is expected:
            // {"method": "example", "timestamp": 1234, "value": 687, "id": 10}
            // See: https://arduinojson.org/v6/how-to/do-serial-communication-between-two-boards/

            // Add values to the document.
            response_doc["timestamp"] = doc["timestamp"].as<long>();
            response_doc["value"] = doc["value"].as<int>();

          // Pong method.
          } else if (method == "ping"){
            response_doc["result"] = "pong";

          // Acquire data method.
          } else if (method == "read"){
            updateSensorReadings();
            response_doc["result"] = "done";
            response_doc["sensor_readings"] = make_state();

          // Get readings method.
          } else if (method == "get") {
            // Check if received message is "getReadings"
            Serial.println("Getting readings...");
            response_doc["sensor_readings"] = make_state();

          } else if (method == "blank") {
            // Check if received message is "blank" and update the blank values.
            updateBlanks();
            response_doc["blanks"] = blanks;

          } else if (method == "config") {
            // Update sensor parameters.
            Serial.println("Getting config values...");
            // Extract values from JSON object
            String gain = doc["gain"];
            String time = doc["time"];
            // Update sensor configuration
            updateSensorConfig(gain, time);
            response_doc["sensor_config"] = sensor_config;

          } else if (method == "wifi") {
            // Update wifi credentials.
            Serial.println("Getting new WiFi values...");
            // Extract values from JSON object
            String ssid = doc["ssid"];
            String password = doc["password"];
            // Update wifi configuration
            updateWiFiConfig(ssid, password);

          } else if (method == "factor") {
            // Update wifi credentials.
            Serial.println("Getting new calibration values...");
            // Extract values from JSON object
            String factor = doc["factor"];
            // Update calibration factor configuration
            updateCalibrationConfig(factor);

          } else if (method == "restart") {
            // Update wifi credentials.
            Serial.println("Restarting...");
            restart_esp();

          } else {
            // Handle unknown method key.
            response_doc["message"] = "Unknown method.";
          }

        // Handle missing method key.
        } else {
          response_doc["message"] = "No method selected";
        }

        // Generate the minified JSON and send it to the Serial port.
        serializeJson(response_doc, Serial);
        // Start a new line.
        Serial.println();

      // Deserialization error handler.
      } else {
        // If parsing failed, respond with a JSON document with information.
        JsonDocument err_doc;
        String message = "deserializeJson() returned";
        err_doc["method"] = "error";
        err_doc["data"] = message + err.c_str();
        serializeJson(err_doc, Serial);
        Serial.println();

        // Flush all bytes in the "link" serial port buffer.
        while (Serial.available() > 0)
          Serial.read();
      }
    }
    // Parse the next message until the buffer is empty.
  }
}

// Echo messages received through serial.
void echo_serial(){
  // Send data only when you receive data: https://gist.github.com/Protoneer/96db95bfb87c3befe46e
  int incomingByte = 0;    // for incoming serial data
  if (Serial.available() > 0) {
    while(Serial.available() > 0){
      // read the incoming byte:
      incomingByte = Serial.read();
      // say what you got:
      Serial.print((char)incomingByte);
    }
    // Serial.println();
  }
}


// Send alive message if a second has elapsed.
float timestamp = 0;
void send_alive(){
  float now = millis();
  if(now > timestamp + 5000){
    Serial.println("{\"method\":\"status\",\"data\":\"ok\"}");
    timestamp = now;
  }
}

// Parse incoming messages and run other JSON-RPC logic.
void parse_json_rpc(){
  // Run the JSON-RPC Block if there is a live Serial connection.
  if(Serial){
    // Send alive signal.
    send_alive();
    
    // Echo incoming serial messages, for debugging.
    // echo_serial();

    // Parse JSON messages.
    parse_json_from_serial();
  }
}

void log(const char * message, const char * level){
  JsonDocument info_doc;
  info_doc["method"] = "log";
  info_doc["data"]["level"] = "info";
  info_doc["data"]["message"] = message;
  serializeJson(info_doc, Serial);
  Serial.println();
}

void log_info(const char * message){
  log(message, "info");
}
