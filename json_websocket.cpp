#include <ESPAsyncWebServer.h>
#include <ArduinoJson.h>
#include "LittleFS.h"
#include <Update.h>
// Web interface for firmware upload.
// See: https://randomnerdtutorials.com/esp32-ota-elegantota-arduino/
#include "src/ElegantOTA/src/ElegantOTA.h"

#include "json_websocket.h"

#include "WiFiSetup.h"
// Filesystem tools
#include "LittleFSHandler.h"
// Colormeter tools
#include "Colormeter.h"
// Get: data objects
#include "shared_objects.h"

// Define the file path for the CSV
#define SAVED_DATA_FILE_PATH "/measurements.csv"

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create a WebSocket object
AsyncWebSocket websocket_1("/ws");

void notifyClients(String data) {
  // Send sensor readings.
  websocket_1.textAll(data);
}

void cleanupClients() {
  // Iterates through all the connected WebSocket clients,
  // and removes those that are no longer active or connected
  websocket_1.cleanupClients();
}

void sendRestartAlert(){
  // Create a temporary buffer for JSON serialization.
  JsonDocument alert_doc;
  // Add gain and time information.
  alert_doc["alert"] = "Now restarting.";
  // Create a String to hold the JSON output.
  String alert_data;
  // Serialize JSON to String.
  serializeJson(alert_doc, alert_data);

  // Alert all clients.
  notifyClients(alert_data);
}

// Main websocket event handler.
void handleWebSocketMessage(void *arg, uint8_t *data, size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo*)arg;
  if (info->final && info->index == 0 && info->len == len && info->opcode == WS_TEXT) {

    String receivedMessage = "";
    for (size_t i = 0; i < len; i++) {
      receivedMessage += (char)data[i];
    }

    // Parse received JSON message
    JsonDocument event_data; // Adjust size according to your JSON message size
    DeserializationError error = deserializeJson(event_data, receivedMessage);
    if (error) {
      Serial.print("JSON parsing error: ");
      Serial.println(error.c_str());
      return;
    }

    // Check if received message is "getReadings"
    if (event_data.containsKey("action")) {
      String action = event_data["action"];

      if (action == "getReadings") {
        // Check if received message is "getReadings"
        Serial.println("Getting readings...");
        String sensorReadings = serialize_state();
        //Serial.println(sensorReadings);
        notifyClients(sensorReadings);

      } else if (action == "blank") {
        // Check if received message is "blank" and update the blank values.
        updateBlanks();

      } else if (action == "config") {
        // Update sensor parameters.
        Serial.println("Getting config values...");
        // Extract values from JSON object
        String gain = event_data["gain"];
        String time = event_data["time"];
        // Update sensor configuration
        updateSensorConfig(gain, time);

      } else if (action == "wifi") {
        // Update wifi credentials.
        Serial.println("Getting new WiFi values...");
        // Extract values from JSON object
        String ssid = event_data["ssid"];
        String password = event_data["password"];
        // Update wifi configuration
        updateWiFiConfig(ssid, password);

      } else if (action == "factor") {
        // Update wifi credentials.
        Serial.println("Getting new calibration values...");
        // Extract values from JSON object
        String factor = event_data["factor"];
        // Update calibration factor configuration
        updateCalibrationConfig(factor);

      } else if (action == "restart") {
        // Update wifi credentials.
        Serial.println("Restarting...");
        // Restart the ESP8266.
        sendRestartAlert();
        delay(2000);
        restart_esp();
      }
    }
  }
}


void websocket_handler(AsyncWebSocket *server, AsyncWebSocketClient *client, AwsEventType type, void *arg, uint8_t *data, size_t len) {
  switch (type) {
    case WS_EVT_CONNECT:
      Serial.printf("WebSocket client #%u connected from %s\n", client->id(), client->remoteIP().toString().c_str());
      break;
    case WS_EVT_DISCONNECT:
      Serial.printf("WebSocket client #%u disconnected\n", client->id());
      break;
    case WS_EVT_DATA:
      // Main websocket event handler.
      handleWebSocketMessage(arg, data, len);
      break;
    case WS_EVT_PONG:
    case WS_EVT_ERROR:
      break;
  }
}

void initWebSocket() {
  // Register event handler function.
  websocket_1.onEvent(websocket_handler);
  // Register websocket as handler in the web server.
  server.addHandler(&websocket_1);
}

void setup_webserver(){
  // Web socket setup.
  initWebSocket();

  // Web Server Root URL
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(LittleFS, "/index.html", "text/html");
  });

  // Web Server Config URL
  server.on("/config", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(LittleFS, "/config.html", "text/html");
  });

  // Endpoint to download the CSV file
  server.on("/download", HTTP_GET, [](AsyncWebServerRequest *request) {
    if (LittleFS.exists(SAVED_DATA_FILE_PATH)) {
      request->send(LittleFS, SAVED_DATA_FILE_PATH, "text/csv");
    } else {
      request->send(404, "text/plain", "File not found.");
    }
  });

  // Endpoint to download the CSV file
  server.on("/update", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(LittleFS, "/update.html", "text/html");
  });

  ElegantOTA.begin(&server);

  server.serveStatic("/", LittleFS, "/");

  // Start server
  server.begin();
}
