#include <Arduino.h>

// RTC: https://docs.espressif.com/projects/esp-idf/en/stable/esp32s3/api-reference/peripherals/gpio.html#api-reference-rtc-gpio
#include "driver/rtc_io.h"  // Required for RTC GPIO functions
#define BUTTON_PIN_BITMASK(GPIO) (1ULL << GPIO)  // 2 ^ GPIO_NUMBER in hex
#define WAKEUP_GPIO              GPIO_NUM_0     // Only RTC IO are allowed - ESP32 Pin example (33)
RTC_DATA_ATTR int bootCount = 0;

unsigned long lastInteractionTime = 0;
unsigned long backlightOffTime = 3 * 60 * 1000; // 3 minutes
unsigned long sleepTime = 10 * 60 * 1000;       // 7 minutes after backlight off

void sleep_tick(){
  unsigned long currentTime = millis();
  bool backlight_sleep = false;
  bool esp_sleep = false;

  // Check if it's time to disable the backlight
  if (!backlight_sleep && (currentTime - lastInteractionTime > backlightOffTime)) {
    backlight_sleep = true;
    lastInteractionTime = millis(); // Reset timer for sleep countdown
  }

  // Check if it's time to enter sleep mode
  if (currentTime - lastInteractionTime > sleepTime) {
    esp_sleep = true;
  }
}

// Function to enter sleep mode
void enterSleepMode(int button_pin) {
  // Cast button_pin to gpio_num_t
  gpio_num_t gpio_pin = static_cast<gpio_num_t>(button_pin);

  if(rtc_gpio_is_valid_gpio(gpio_pin)){
    Serial.println("Entering sleep mode...");
  } else {
    // Invalid pins: GPIO_NUM_48, GPIO_NUM_47, ...
    Serial.print("Invalid wakeup pin: ");
    Serial.print(gpio_pin);
    Serial.println("wake-up may not work.");
    // return;
  }

  /*
    First we configure the wake up source
    We set our ESP32 to wake up for an external trigger.
    There are two types for ESP32, ext0 and ext1.
    
    ext0 uses RTC_IO to wakeup thus requires RTC peripherals
    to be on while ext1 uses RTC Controller so does not need
    peripherals to be powered on.
    
    Note that using internal pullups/pulldowns also requires
    RTC peripherals to be turned on.
  */

  // Configure the specified button pin as wakeup source
  // https://docs.espressif.com/projects/esp-idf/en/stable/esp32s3/api-reference/peripherals/gpio.html#api-reference-rtc-gpio
  esp_sleep_enable_ext0_wakeup((gpio_num_t)gpio_pin, LOW);  // 1 = High, 0 = Low
  // https://docs.espressif.com/projects/esp-idf/en/stable/esp32/api-reference/system/sleep_modes.html
  // esp_sleep_enable_ext1_wakeup_io(BUTTON_PIN_BITMASK(gpio_pin), ESP_EXT1_WAKEUP_ALL_LOW); // Or: ESP_EXT1_WAKEUP_ANY_HIGH

  // Enable pull-down resistor.
  // rtc_gpio_pullup_dis(gpio_pin);
  // rtc_gpio_pulldown_en(gpio_pin);

  // Enable pull-up resistor.
  rtc_gpio_pulldown_dis(gpio_pin);
  rtc_gpio_pullup_en(gpio_pin);

  // Init a GPIO as RTC GPIO. 
  // This function must be called when initializing a pad for an analog function.
  // rtc_gpio_init((gpio_num_t)gpio_pin);

  // Set the pin direction as input
  // https://docs.espressif.com/projects/esp-idf/en/stable/esp32s3/api-reference/peripherals/gpio.html#id8
  // rtc_gpio_set_direction((gpio_num_t)gpio_pin, RTC_GPIO_MODE_INPUT_ONLY);
  // rtc_gpio_pullup_en((gpio_num_t)gpio_pin);

  // Enter deep sleep
  delay(1000); // Add a delay, prevents immediate restarts if the button is not released.
  esp_deep_sleep_start();
}

void setup_sleep(){
  //Increment boot number and print it every reboot
  ++bootCount;
}
