# Simple ESP8266 WebUI for IORodeo's colorimeter sensor hardware

Code for:

- Reading from the TSL2591 sensor (through I2C).
- Serving a webpage with sensor readings.
- Updating the webpage every 1 second with new readings, through websocket.

## Gallery

> The web ui, showing raw measurements, transmittance, absorbance, and lux.

![alt text](docs/images/webui.png)![alt text](docs/images/webui_config.png)

> Models for the case are available in the [models](./models/esp8266-case/) directory.

![alt text](docs/images/wireless.jpeg)

> The prototype case contains a NodeMCU development board.

![alt text](docs/images/hardware.png)

> There is also a newer version using an ESP32.

![alt text](docs/images/new_case.jpeg)

## Usage

### Standalone

Connect the ESP8266 to a USB power supply using a micro-USB cable.

The built-in LED will blink during a measurement. This is for visual feedback and aesthetic purposes only.

The web ui can be accessed by connecting to the ESP8266's WiFi access point (SSID: `Colormeter595`, PASS: `passwowrd`), and then visiting <http://192.168.4.1>.

Click on the "Blank" button to use the last measurement as blank values. These are used to calculate transmittance and absorbance.

#### Transmittance

Instructions:

1. Open the web-UI, either through the AP or the configured network.
2. Insert the reference sample, and press "blank".
3. Insert the sample of interest, and note the transmittance value.

#### Absorbance

Instructions:

1. Open the web-UI, either through the AP or the configured network.
2. Insert the reference sample, and press "blank".
3. Insert the sample of interest, and note the absorbance value.

> The absorbance measurement is shown in the OLED display too.

#### Optical density

OD measurement is also called turbidimetry, or nephelometry. It is a measurement of light scattering off the sample, which requires an off-axis sensor (i.e. more than 0º off the illumination axis).

In spite of this, the OD of a microorganism suspension is equated to its absorbance at 600 nm, which is an _indirect_ measurement of how much light was scattered by the sample. That absorbance-based "pseudo-OD" measurement is a good enough proxy of the "proper" OD, based on scattering.

However, the absorbance of a scattering sample _will not_ be the same across devices. This is why a linear correction factor needs to be calculated, saved to the micro-controller, and used henceforth to correct the absorbance measurements.

Here is some reading material:

- <https://blog.iorodeo.com/turbidity-prototype/>
- <https://assets.thermofisher.com/TFS-Assets/MSD/Technical-Notes/TN52236-differences-bacterial-optical-density-uv-vis.pdf>

#### Adjustment factor

By default, the absorbance is reported as calculated.

As explained above, the absorbance measurements of turbid samples will vary between instruments.

If an adjustment factor is set through the web-UI, it will be used to scale the absorbance reading by multiplying it. This is an attempt to compensate for those variations.

To make the value match your previous absorbance measurements, make a measurement in both devices, using the same blank and sample.

Divide the value shown by your previous device by the value shown by your new ODi colormeter, this is the adjustment factor. Use the web-UI configuration page to save its value in the colorimeter.

For `OD600=0.6` in a Genesys spectrometer, we have used adjustment factors between `1.20` and `1.43`.

### Programmatic

There are two ways to interact with the device programmatically, through "API"s:

1. [Websocket API](json_websocket.cpp), accessible through WiFi. Methods implemented by `handleWebSocketMessage`.
2. [Serial API](json_rpc.cpp) through USB, using stringified JSON messages. Methods implemented by `parse_json_from_serial`.
3. Additionally, there are [HTTP endpoints](#http-endpoints) for static files and downloads.

For example, sending `{"method": "restart"}` through the serial interface will restart the device.

Sending `{"id": 33, "method": "getReadings"}` through the Websocket will return the latest readings data:

```json
{
  "id": 33,
  "sensor_readings": {
    "full_lum": 4534,
    "ir_lum": 585,
    "vis_lum": 3949,
    "lux_lum": 112.2646179,
    "full_trans": 0.998238683,
    "ir_trans": 0.998293519,
    "vis_trans": 0.998230517,
    "full_abs": 0.000934038,
    "ir_abs": 0.000904933,
    "vis_abs": 0.000938372,
    "current_gain": "TSL2591_GAIN_MED",
    "current_time": "TSL2591_INTEGRATIONTIME_500MS",
    "adj_factor": 1.220000029
  }
}
```

### HTTP Endpoints

When accessing the colorimeter's IP, the `index.html` file is served, this is at the "root" endpoint (i.e. `/`).

The `/config` endpoint returns the configuration page `config.html`.

Other URLs return possibly useful things:

- `/measurements.csv` returns the saved measurements.
- All files in the [data](./data/) directory are uploaded to the ODi's file system, and can be downloaded by URL.
    - `/wifi.json`: JSON file for the WiFi credentials.
    - `/blank.json`: JSON file for the blank values in use.
    - `/config.json`: JSON file with configuration parameters for the sensor.
    - `/calibration.json`: JSON file with adjustment factor for absorbance measurements.
    - Other files in the [data](./data/) directory.

### Firmware Update

Browse to your device's IP, loading the updater page: <http://192.168.4.1/update>

Upload a binary file, and restart the ESP32 manually.

A binary file can be found at the [build](build/) directory:

- First version working with OTA: [esp8266-iorodeo-colormeter.ino.bin](build/esp32.esp32.esp32s3/esp8266-iorodeo-colormeter.ino.bin)

## Downloads

Use `git` to clone the repository and its submodules:

```bash
git clone --recurse-submodules

# If you've already cloned the repository without submodules, initialize them manually:
# git submodule update --init --recursive
```

To update the repository and its submodules:

```bash
git pull --rebase
git submodule update --recursive --remote
```

## Installation

Full installation steps using the Arduino IDE.

### Arduino IDE

- Arduino IDE setup (v2+):
  - Install the IDE, and add yourself to the `uucp` or `dialout` user groups.
  - Add ESPs to the board manager.
    - ESP32 (version 3.1.1): <https://docs.espressif.com/projects/arduino-esp32/en/latest/installing.html>
    - ESP8266: <https://github.com/esp8266/Arduino>
  - Install the LittleFS plugin to the Arduino IDE (tutorials below).

### Arduino libraries

- AsyncTCP (version 1.1.4)
- ESPAsyncTCP (version 1.2.4)
- ESP Async WebServer (ESPAsyncWebServer version 1.2.4)
  - If compiling for ESP32 with an arduino-esp32 board version 3.0.4 (or greater) you may need to [install an alternate library](#espasyncwebserver).
- ESP8266WiFi (version 1.0)
- PubSubClient (version 2.8 by https://pubsubclient.knolleary.net/)
- LittleFS (version 0.1.0)
- ElegantOTA
  - **IMPORTANT**: Install our fork of this library, as [explained below](#elegantota-library).
- FastLED (version 3.9.7)
  - This one causes a spurious error and a reboot. Using Adafriut's library for now.
- Adafruit_NeoPixel (version 1.12.4)
- ArduinoJson (version 7.1.0)
- Adafruit Unified Sensor (version 1.1.14)
- Adafruit TSL2591 Library (version 1.4.5)
  - **IMPORTANT**: Install our fork of this library, as [explained below](#adafruit-tsl2591-library).
- NTPClient (version 3.2.1 from https://github.com/arduino-libraries/NTPClient)
- OneButton (version 2.6.1)
- Display libraries:
  - Classic I2C OLED display:
    - Adafruit SSD1306 (version 2.5.11)
    - Adafruit GFX Library (version 1.11.10)
    - Adafruit BusIO (version 1.16.1)
  - TFT display on the Lolin S3 Mini Pro:
    - Arduino_GFX / Arduino_GFX_Library: GFX Library for Arduino (version 1.4.9) 
- Wire
- SPI
- WebSockets (version 2.6.1)

#### ElegantOTA Library

Use this fork: <https://gitlab.com/naikymen/ElegantOTA.git>

Changes:

- Enable async by default.
- Let the user define the `/update` endpoint.
- Cleaner webpage.

The library is [bundled as a git submodule](#downloads).

#### Adafruit TSL2591 Library

Install our fork of this library, instead of the official one: <https://github.com/naikymen/Adafruit_TSL2591_Library/tree/patch-1>

The [need for this](https://github.com/adafruit/Adafruit_TSL2591_Library/pull/54) is explained at adafruit's github repo.

The library is [bundled as a git submodule](#downloads).

These manual installations are no longer needed, and are left here for reference:

1. Open a terminal, and change to the Arduino Libraries directory (e.g. `~/Arduino/libraries/`).
2. Move or delete the original Adafruit TSL2591 Library: `mv Adafruit_TSL2591_Library ../Adafruit_TSL2591_Library.orig`
3. Clone our fork: `git clone -b patch-1 https://github.com/naikymen/Adafruit_TSL2591_Library.git Adafruit_TSL2591_Library`

To revert, delete the `Adafruit_TSL2591_Library` directory, and re install the original library through the Arduino IDE.

#### ESPAsyncWebServer

Use the library from `me-no-dev`'s git repository, instead of the one listed on the ArduinoIDE's library manager.

1. Open your Arduino library folder
1. Remove or rename any existing `ESPAsyncWebServer` directory.
1. Install the library by running: `git clone https://github.com/me-no-dev/ESPAsyncWebServer.git`
  - Note: there is a new repo for this library: <https://github.com/ESP32Async/ESPAsyncWebServer>

### Prepare for Flashing

To flash the firmware on an "ESP8266 12-E NodeMCU Kit", skip this part.

To flash firmware on the Wemos Lolin S3 Mini Pro (or ESP32-S3 and variants) you may need to:
- Set "USB CDC on Boot" to "Enabled" under "Tools" in the Arduino IDE.
- Set the board to "ESP32 S3 dev" instead of "Lolin S3 Mini Pro".
- Put the device in bootloader/download mode by holding IO0 (first button), pressing reset (or reconnecting power), and releasing IO0.
- You might need to set `Flash Mode` to `DIO` instead of `QIO` ([ref.](https://github.com/HomeSpan/HomeSpan/discussions/490#discussioncomment-5976623)).

Notes:

- The pin variables for the ESP32-S3, including SPI pins, are [defined here](https://github.com/espressif/arduino-esp32/pull/10021/files#diff-9226f611f701f95944e04d946801066681c1461cd482967e25cc66f0e5e62ace).
- Note that a pin with name `GPIO66` is referenced as pin `66` in the ArduinoIDE.
- There are some test sketches to get you going [here](./tests).

### Flash the Firmware

- Flash the firmware from the Arduino IDE.
  - Select the appropriate board and port.
  - You'll need all libraries installed, see below.
  - Compile and Upload the sketch.

### Flashing via arduino-cli

First install `arduino-cli` in your system, and all the required libraries (e.g. through the IDE).

Official documentation:

- Setup: https://arduino.github.io/arduino-cli/1.1/getting-started/
- Compilation: https://arduino.github.io/arduino-cli/1.1/commands/arduino-cli_compile/
- Flashing: https://arduino.github.io/arduino-cli/1.1/commands/arduino-cli_upload/

```bash
# Setup: https://arduino.github.io/arduino-cli/1.1/getting-started/
arduino-cli config init
arduino-cli core update-index
arduino-cli board listall esp32

# Compilation: https://arduino.github.io/arduino-cli/1.1/commands/arduino-cli_compile/
arduino-cli compile --show-properties --fqbn esp32:esp32:esp32s3 # Shows all properties
arduino-cli compile --fqbn esp32:esp32:esp32s3 --build-properties build.defines="-DUSB_CDC_ON_BOOT"

# Flashing: https://arduino.github.io/arduino-cli/1.1/commands/arduino-cli_upload/
arduino-cli board list # Look for the ESP's serial port.
ls -l /dev/serial/by-id/ # Look for the ESP's serial port.
arduino-cli upload -p /dev/ttyACM0 --fqbn esp32:esp32:esp32s3 # --verbose
```

To generate a list of dependencies:

```bash
# Show used libraries.
arduino-cli compile --only-compilation-database --fqbn esp32:esp32:esp32s3
arduino-cli lib list

# Save a list of all installed libraries.
# arduino-cli lib list > libraries.txt
```

Output:

    Used library             Version
    ArduinoJson              7.3.0
    OneButton                2.6.1
    GFX Library for Arduino  1.5.0 (1.5.2)
    ESP Async WebServer      1.2.4 (me-no-dev's git)
    AsyncTCP                 1.1.4
    WiFi                     3.0.7
    Networking               3.0.7
    Adafruit Unified Sensor  1.1.15
    Adafruit TSL2591 Library 1.4.5
    Adafruit BusIO           1.16.3
    FastLED                  3.9.11
    Adafruit GC9A01A         1.1.1
    Adafruit NeoPixel        1.12.3
    Adafruit SSD1306         2.5.13
    ESPAsyncTCP              1.2.4
    TFT_eSPI                 2.5.43
    WebSockets               2.6.1

    Used platform Version
    esp32:esp32   3.0.7

### Upload files to LittleFS 

First install the upload plugin, if you havent.

- Download the `.vsix` file from <https://github.com/earlephilhower/arduino-littlefs-upload/releases>
- Move the `.vsix` file to the plugin folder of the Arduino IDE (v2+).
  - The plugin folder might be `.arduinoIDE/plugins` or `.arduino15/plugins` at the home directory.
- Restart the Arduino IDE.

Once the plugin is installed, use the LittleFS plugin to upload the files in [data](./data) to the ESP's flash (see installation [tutorials](#tutorials) below).

- Press `Ctrl+Shift+P` to open the command palette, start typing `Upload LittleFS to ...` and press enter when the command is selected in the list.
- You may need to adjust the flash size first in `Tools -> Flash Size` (e.g. `4MB (FS:2M OTA:~1019KB)`).

## Wiring

You'll need to fashion one I2C adapter cable out of a Qwiic/StemmaQT cable.

### NodeMCU - ESP8266

The ESP8266 MCU only has one I2C "port", and no onboard display.

You'll need to make an I2C cable adapter out of a Qwiic/StemmaQT cable, that can connect to the pin header on the board.

1. Cut the Qwiic/StemmaQT end of the cable.
2. Crimp dupont connectors on the free wires.
3. Connect the 0.96' OLED display to the ESP8266.

### Lolin S3 Mini Pro - ESP32-S3

The Lolin S3 Mini Pro (and variants) use the same 4-wire connector, but the wiring is different.

Unfortunately, the wiring on JST SH1.0-4P connector on the Lolin is incompatible with the wiring on STEMMA-QT/Qwiic I2C connectors on Adafruit/Sparkfun stuff, even though it physically fits.

Here is how to make an adapter out of an existing Qwiic/StemmaQT cable:

1. Cut the red (🔴), black (⚫) and yellow (🟡) wires half-way of the cable, and **do not cut** the blue wire (🔵).
2. Peel off 2 mm of insulation on all free wire ends (🔴⚫🟡).
3. Grab both ends of the connector with "helping hands" (optional) this will help keep track of which side is left and which right.
4. Turn the blue wire (🔵) around the free wires (🔴⚫🟡-right) on the right side of the connector due to the length difference (optional).
5. Cut 3 pieces heat-shrink, each 8 mm long.
6. Place the heatshrink over the free wires on the left side of the connector (🔴⚫🟡-left).
7. Add solder to each wire.
8. Solder the cables as indicated:
    - Red (🔴-left) to black (⚫-right).
    - Yellow (🟡-left) to red (🔴-right).
    - Black (⚫-left) to yellow (🟡-right).
    - *Tip*: you only really need to remember red-to-black, and then realise that yellow shouldn't go with yellow.

Wiring diagram:

![cable_adapter](docs/images/stemaqt_qwiic-lolin_i2c_SH1.0-4P.svg)

Reading material:

- <https://learn.adafruit.com/introducing-adafruit-stemma-qt/technical-specs>
- <https://learn.adafruit.com/introducing-adafruit-stemma-qt/what-is-stemma-qt>
- <https://docs.wemos.cc/en/latest/_static/files/sch_d1_mini_pro_v2.0.0.pdf>

## Development

We've tried to keep the main sketch short and simple: [esp-iorodeo-colormeter.ino](esp-iorodeo-colormeter.ino)

The code is split into different files for modularity.

- [Colormeter.cpp](Colormeter.cpp): Functions to manage the sensor and make measurements.
- [WiFiSetup.cpp](WiFiSetup.cpp): Functions to manage the WiFi connection and web server.
- [LittleFSHandler.cpp](LittleFSHandler.cpp): Functions to manage persistent storage.
- [OLED.cpp](OLED.cpp): Functions to manage the OLED display.
- [shared_objects.cpp](shared_objects.cpp): Shared data objects.
- [json_websocket.cpp](json_websocket.cpp): Websocket API.
- [json_rpc.cpp](json_rpc.cpp): Serial API.

There are data objects (JsonDocuments) that are shared by all modules, defined in [shared_objects.cpp](shared_objects.cpp).

I2C pin customization:

- The sensor's I2C pins can be changed in the [main sketch file](esp-iorodeo-colormeter.ino).
- The OLED's I2C pins and display parameters must be the same, because the ESP8266 only has one I2C interface.
- If your board has an additional I2C bus, the display's pins can be changed by editing the [OLED.h](OLED.h) and [OLED.cpp](OLED.cpp) files.
  - Follow the comments therein.

### Color picker

Color picker to generate 16-bit hexadecimal color values, for Adafruit's library: <https://rgbcolorpicker.com/565>

### Reading material

Useful reads for this project:

- Pinout for the ESP8266 12-E NodeMCU Kit pinout:
  - <https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/>
  - <https://lastminuteengineers.com/esp8266-pinout-reference/>
- LED hardware: <https://github.com/iorodeo/i_control_led>
- Sensor boards: <https://blog.iorodeo.com/light-sensor-boards/>
- Sensor wiring and usage: <https://learn.adafruit.com/adafruit-tsl2591/wiring-and-test>
- Adafruit GFX library reference: <https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives>

### Tutorials

- Websockets:
  - <https://randomnerdtutorials.com/esp8266-nodemcu-websocket-server-sensor/>
- LittleFS upload plugin:
  - <https://randomnerdtutorials.com/arduino-ide-2-install-esp8266-littlefs/>
  - <https://randomnerdtutorials.com/install-esp8266-nodemcu-littlefs-arduino/>
  - <https://github.com/earlephilhower/arduino-littlefs-upload?tab=readme-ov-file#installation>
  - <https://github.com/earlephilhower/arduino-littlefs-upload>
  - <https://github.com/earlephilhower/arduino-littlefs-upload/releases>
- mDNS: <https://lastminuteengineers.com/esp32-mdns-tutorial/>

### Test web server

Using `npm` you can serve the website locally, with very limited functionality:

```bash
npm install http-server
npx http-server data/
```

And browse to: <http://127.0.0.1:8080/update>

## To-do

Issue tracker: <https://gitlab.com/pipettin-bot/equipment/esp-iorodeo-colormeter/-/boards>

## Troubleshooting

- No output in the serial monitor: enable "USB CDC On Boot" in the Arduino IDE and reflash the firmware.

### Buttons don't trigger

Buttons doing nothing can be caused by long "delays" in the code.

The interrupts used for buttons appear to have a timeout of a few milliseconds. If a button is pressed during a long delay in the code, the onebutton won't pick up the interrupt, because it has already been discarded.

This appears to have been resolved by replacing the "delay" on the adafruit TSL library with a FreeRTOS "async" delay, which does not block the main `loop()`.

### Reboots due to FastLED stuff:

These errors might appear while running:

```
E (120375) rmt: rmt_tx_register_to_group(132): no free tx channels
E (120375) rmt: rmt_new_tx_channel(239): register channel failed
ESP_ERROR_CHECK failed: esp_err_t 0x105 (ESP_ERR_NOT_FOUND) at 0x4201e613
file: "/home/nicomic/Arduino/libraries/FastLED/src/platforms/esp/32/led_strip/rmt_strip.cpp" line 74
func: void fastled_rmt51_strip::RmtLedStrip::acquire_rmt()
expression: err

abort() was called at PC 0x4037f143 on core 1
```

They appear to have been caused by a part of the code that executed the fastled code _too fast_. This appeared after fixing the "delay" issue in the adafruit library.

```
E (291912) rmt: rmt_tx_enable(697): channel not in init state
E (291912) led_strip_rmt: led_strip_rmt_refresh_async(103): enable RMT channel failed
ESP_ERROR_CHECK failed: esp_err_t 0x103 (ESP_ERR_INVALID_STATE) at 0x4201eb49
file: "/home/nicomic/Arduino/libraries/FastLED/src/platforms/esp/32/rmt_5/strip_rmt.cpp" line 105
func: virtual void {anonymous}::RmtStrip::drawAsync()
expression: led_strip_refresh_async(mStrip)
```

Similar issue, but for the ESP32-C3 (not the S3): <https://github.com/FastLED/FastLED/issues/1748>

### undefined reference to `pxCurrentTCB'

See: https://github.com/me-no-dev/ESPAsyncWebServer/issues/1442

Unable to compile using ESP32 board version `3.1.2`:

    .arduino15/packages/esp32/tools/esp-x32/2405/bin/../lib/gcc/xtensa-esp-elf/13.2.0/../../../../xtensa-esp-elf/bin/ld: /home/nicomic/.cache/arduino/sketches/F6A0144FC7B85C2272D5D7A98267366F/libraries/ESPAsyncWebServer/AsyncWebSocket.cpp.o:(.literal._ZN17AsyncWebLockGuardC5ERK12AsyncWebLock[_ZN17AsyncWebLockGuardC5ERK12AsyncWebLock]+0x0): undefined reference to `pxCurrentTCB'

Install board version `3.0.7`.
