
#include <Arduino.h>
#include <math.h>

#include "Colormeter.h"

#define NUM_SAMPLES 30       // Number of data points to collect
#define SAMPLE_INTERVAL 500  // Time in milliseconds between samples
#define TOLERANCE 0.01       // 1% tolerance for equilibrium estimation

float intensities[NUM_SAMPLES];  // Stores intensity measurements
unsigned long timestamps[NUM_SAMPLES];  // Stores time in milliseconds
bool calibrationDone = false;
float I_inf = 0, k = 0;  // Equilibrium intensity and decay constant
unsigned long t_eq = 0;  // Equilibrium time in milliseconds

// Fit exponential decay model I(t) = I_inf + (I0 - I_inf) * exp(-k*t)
void fitExponentialDecay() {
    float sum_time = 0, sum_I = 0, sum_tI = 0, sum_tt = 0, sum_lnI = 0;
    
    // Compute logarithm of intensity differences
    float I0 = intensities[0];  // Initial intensity
    for (int i = 0; i < NUM_SAMPLES; i++) {
        if (intensities[i] <= 0) continue; // Avoid log of zero or negative values
        sum_time += timestamps[i];
        sum_I += intensities[i];
        sum_tt += timestamps[i] * timestamps[i];
        sum_lnI += log(intensities[i]);
        sum_tI += timestamps[i] * log(intensities[i]);
    }

    // Solve for k using linear regression: ln(I - I_inf) = -kt + C
    float n = NUM_SAMPLES;
    k = -(n * sum_tI - sum_time * sum_lnI) / (n * sum_tt - sum_time * sum_time);

    // Estimate equilibrium intensity I_inf as the last measured value
    I_inf = intensities[NUM_SAMPLES - 1];

    // Compute equilibrium time when I(t) reaches 99% of I_inf
    t_eq = -log(TOLERANCE) / k;

    // Mark calibration as done.
    calibrationDone = true;
}

// Function to fit the reciprocal model: I(t) = a + b/t
// Fit using least squares: https://en.wikipedia.org/wiki/Simple_linear_regression#Expanded_formulas
void fitReciprocalModel(float* intensities, unsigned long* timestamps, int num_samples, float& a, float& b) {
    float sum_1_t = 0, sum_1_t2 = 0;
    float sum_I = 0, sum_I_1_t = 0;

    // Collect sums for linear regression
    for (int i = 0; i < num_samples; i++) {
        // Convert time to seconds, adding an offset of 1 to stabilize "1/x".
        float t = 1.0 + (timestamps[i] - timestamps[0]) / 1000.0;
        // Reciprocal of time
        float inv_t = 1.0 / t;
        
        sum_1_t += inv_t;
        sum_1_t2 += inv_t * inv_t;
        sum_I += intensities[i];
        sum_I_1_t += intensities[i] * inv_t;
    }

    // Compute coefficients using least squares
    float denom = (num_samples * sum_1_t2) - (sum_1_t * sum_1_t);
    if (denom != 0) {
        a = (sum_I * sum_1_t2 - sum_1_t * sum_I_1_t) / denom;
        b = (num_samples * sum_I_1_t - sum_1_t * sum_I) / denom;
    } else {
        a = 0;
        b = 0;
    }
}

void fit_thermal_calibration() {
    // Logs.
    Serial.println("Starting LED warm-up calibration...");
    Serial.printf("Taking %d samples at %d ms intervals.\n", NUM_SAMPLES, SAMPLE_INTERVAL);

    // // Collect initial intensity data
    // for (int i = 0; i < NUM_SAMPLES; i++) {
    //     intensities[i] = (float)simpleRead();
    //     timestamps[i] = millis();
    //     Serial.printf("Sample %d: I = %.2f at t = %lu ms\n", i, intensities[i], timestamps[i]);
    //     delay(SAMPLE_INTERVAL);
    // }

    // Collect initial intensity data
    for (int i = 0; i < NUM_SAMPLES; i++) {
        // Record the time before measurement
        unsigned long startTime = millis();
        // Make a measurement.
        intensities[i] = (float)simpleRead();
        // Record the time after measurement
        timestamps[i] = millis();

        // Calculate the time taken for measurement.
        unsigned long elapsedTime = timestamps[i] - startTime;

        // Log results.
        Serial.printf("Sample %d: I = %.2f at t = %lu ms (Measurement took %lu ms)\n", 
                      i, intensities[i], timestamps[i], elapsedTime);

        // Adjusted delay to maintain consistent sampling rate
        unsigned long remainingTime = (SAMPLE_INTERVAL > elapsedTime) ? (SAMPLE_INTERVAL - elapsedTime) : 0;
        delay(remainingTime);
    }

    // Fit the exponential decay curve
    fitExponentialDecay();

    // Display results
    Serial.printf("Estimated Equilibrium Intensity (I_inf): %.2f\n", I_inf);
    Serial.printf("Decay Constant (k): %.5f\n", k);
    Serial.printf("Estimated Equilibrium Time: %.2f min\n", t_eq / 60000.0);
}

float thermal_correction(float rawIntensity) {
    if (calibrationDone) {
        // Apply correction based on estimated I_inf
        float correctedIntensity = rawIntensity - (I_inf - rawIntensity) * exp(-k * (millis() / 1000.0));
        // Output corrected value
        Serial.printf("Raw: %.2f, Corrected: %.2f\n", rawIntensity, correctedIntensity);
        return correctedIntensity;
    } else {
        Serial.println("No calibration to apply. Returning unmodified value.");
        return rawIntensity;
    }
}
