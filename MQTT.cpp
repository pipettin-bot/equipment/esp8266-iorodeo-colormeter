#include <MQTT.h>

#include <WiFi.h>
#include <WiFiClientSecure.h>
#include <PubSubClient.h>

// Data objects.
#include "WiFiSetup.h"
#include "LittleFSHandler.h"
#include "shared_objects.h"

#ifdef ESP8266
    #define DEVICE_NAME "ODi-ESP8266-"
#elif defined(ESP32)
    #define DEVICE_NAME "ODi-ESP32-"
#else
    #define DEVICE_NAME "ODi-???-"
#endif

#define MQTT_TOPIC "measurements/absorbance"
#define MQTT_RETRY_INTERVAL 10000  // Time before retrying MQTT connection (ms)

// MQTT Broker: https://docs.thinger.io/mqtt
const char* mqtt_server = "backend.thinger.io";
const int mqtt_port = 1883;  // Use 8883 for SSL

// Thinger.io MQTT Credentials
#define USERNAME " ... "
#define DEVICE_ID " ... "
#define DEVICE_CREDENTIAL " ... "

// TCP connection manager.
WiFiClient espClient;
// WiFiClientSecure espClient; // Alternative secure connection.
PubSubClient client(espClient);

// Callback function for incoming messages
void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived on MQTT topic: ");
  Serial.println(topic);

  String message;
  for (unsigned int i = 0; i < length; i++) {
    message += (char)payload[i];
  }
  Serial.println("Message data: " + message);
}

// Function to connect to MQTT Broker
unsigned long lastMqttAttempt = 0;
void reconnect_mqtt() {
    if (!client.connected() && millis() - lastMqttAttempt > MQTT_RETRY_INTERVAL){
      lastMqttAttempt = millis();  // Update last attempt time

      Serial.print("Attempting MQTT connection. ");
      String clientId = String(DEVICE_ID);
  
      if (client.connect(clientId.c_str(), USERNAME, DEVICE_CREDENTIAL)) {
          Serial.println("Connected!");
          // client.subscribe(MQTT_TOPIC);  // Subscribe to a topic
      } else {
          Serial.print("Failed to connect to MQTT, rc=");
          // State codes: https://pubsubclient.knolleary.net/api#state
          Serial.println(client.state());
      }
    }
}

void setup_mqtt() {
  client.setServer(mqtt_server, mqtt_port);
  // A pointer to a message callback function called when a 
  // message arrives for a subscription created by this client. 
  client.setCallback(callback);
  // Sets the timeout when reading from the network.
  // This also applies as the timeout for calls to connect. 
  client.setSocketTimeout(1); // 1 second is minimum
}

String serialize_abs(void)
{
  // Create a temporary buffer for JSON serialization.
  JsonDocument tempDoc;
  // Add information.
  tempDoc["vis_lum"] = readings["vis_lum"];         // Luminosity (I)
  tempDoc["vis_lum_blank"] = blanks["vis_lum"];     // Blank value (I0)
  tempDoc["vis_trans"] = readings["vis_trans"];     // Transmittance (I/I0)
  tempDoc["vis_abs"] = readings["vis_abs"];         // Abosrbance (log10(1/T))
  tempDoc["savedCount"] = savedCount;               // Saved measurements count (useful as index)

  // Create a String to hold the JSON output.
  String jsonString;
  // Serialize JSON to String.
  serializeJson(tempDoc, jsonString);

  return jsonString;
}

void loop_client(){
  // Keep MQTT client running
  client.loop();
    
  Serial.print("Publishing state to MQTT: ");
  String payload = serialize_abs();  // Send a reduced state.
  // String payload = serialize_state();  // Send the full state.
  // String payload = "{ \"temperature\": 25.5, \"humidity\": 60 }";  // Example.
  Serial.println(payload);

  // Example: Publish data
  // https://pubsubclient.knolleary.net/api#publish
  if(client.publish(MQTT_TOPIC, payload.c_str())){
    Serial.println("MQTT data published.");
  } else {
    Serial.println("MQTT publish failed!");
  }
  // Note: The public server is rate limited to 1 point per minute.
  // https://community.thinger.io/t/bucket-write-limit-1-point-minute/4918/7
}

void loop_mqtt() {
  // Only send messages if not in AP mode.
  if(is_ap){
    Serial.println("MQTT publish skipped; WiFi is in AP mode (no internet).");
    return;
  }

  // Reconnect if disconnected.
  reconnect_mqtt();

  // Try sending data to the MQTT server.
  if (client.connected()) {
    loop_client();
  } else {
    Serial.println("MQTT publish skipped; client disconnected.");
  }
}
