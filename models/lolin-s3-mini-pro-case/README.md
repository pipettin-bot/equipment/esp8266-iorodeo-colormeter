
Dimensions: https://www.wemos.cc/en/latest/_static/files/dim_s3_mini_pro_v1.0.0.pdf

Models from the internet:

- https://www.printables.com/model/1002665-wemos-lolin-s3-mini-case
- https://www.printables.com/model/669542-lolin-esp32-s3-mini-slim-case
