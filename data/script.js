
var gateway = `ws://${window.location.hostname}/ws`;
var websocket;
// Init web socket when the page loads
window.addEventListener('load', onload);

function onload(event) {
    initWebSocket();
}

// Function to send the "getReadings" event to the server
function getReadings() {
  var message = {
      action: "getReadings"
  };
  websocket.send(JSON.stringify(message));
}

// Function to send the "blank" event to the server
function sendBlankEvent() {
  var message = {
      action: "blank"
  };
  websocket.send(JSON.stringify(message));
}

function initWebSocket() {
    console.log('Trying to open a WebSocket connection...');
    websocket = new WebSocket(gateway);
    websocket.onopen = onOpen;
    websocket.onclose = onClose;
    websocket.onmessage = onMessage;
}

// When websocket is established, call the getReadings() function
function onOpen(event) {
    console.log('Connection opened.');
    // Note: disabled this call. Trying to track where the exceptions in firmware come from.
    //getReadings();
}

function onClose(event) {
    console.log('Connection closed.');
    setTimeout(initWebSocket, 2000);
}

// Function that receives the message from the ESP32 with the readings
function onMessage(event) {
    console.log("Received message:");
    console.log(event.data);
    var myObj = JSON.parse(event.data);
    var keys = Object.keys(myObj);

    for (var i = 0; i < keys.length; i++){
        // Get the key and value from the entry.
        var key = keys[i];
        var val = myObj[key];

        // Trigger alert.
        if(key == "alert"){
            alert(val);
        }

        // Convert value tu number and round if possible.
        var num = Math.round((val + Number.EPSILON) * 100) / 100;
        if(!isNaN(num)){
            val = num;
        }
        // Update the element.
        var element = document.getElementById(key);
        if (element) {
            element.innerHTML = val;
        }
        // } else {
        //     console.log("Element with id '" + key + "' not found.");
        // }
    }
}

function submitConfig() {
  // Get selected values from the form
  var gain = document.getElementById('gain').value;
  var time = document.getElementById('timing').value;

  // Prepare data object
  var configData = {
      action: "config",
      gain: gain,
      time: time
  };

  // Convert data to JSON string
  var jsonData = JSON.stringify(configData);

  // Send data over WebSocket
  websocket.send(jsonData);

  // Optionally, you can handle response or feedback here
  // For example, update UI or show confirmation message
  console.log('Config data sent:', configData);
}

// Function to handle form submission for WiFi configuration
function submitWifiConfig() {
    // Get form data
    const ssid = document.getElementById('ssid').value;
    const password = document.getElementById('password').value;
    
    // Alert the user.
    alert("The device will reset and become available in the '" + ssid + "' network. If it fails to connect, it will create its own AP network. You can connect to it to update or fix the credentials." )

    // Construct data object
    const wifiConfig = {
        action: "wifi",
        ssid: ssid,
        password: password
    };

    // Convert data to JSON string
    var jsonData = JSON.stringify(wifiConfig);

    // Send data over WebSocket
    websocket.send(jsonData);

    // Optionally, you can handle response or feedback here
    // For example, update UI or show confirmation message
    console.log('WiFi credentials sent:', wifiConfig);
}

// Function to handle form submission for WiFi configuration
function submitFactorConfig() {
    // Get form data
    const factor = parseFloat(document.getElementById('factor').value);

    if(isNaN(factor)){
        alert("Form error: only numeric values may be used as calibration factors.")
        return;
    }
    
    // Construct data object
    const factorConfig = {
        action: "factor",
        factor: factor
    };

    // Convert data to JSON string
    var jsonData = JSON.stringify(factorConfig);

    // Send data over WebSocket
    websocket.send(jsonData);

    // Optionally, you can handle response or feedback here
    // For example, update UI or show confirmation message
    console.log('New calibration factor sent:', factorConfig);
}

// Function to handle form submission for WiFi configuration
function sendRestartEvent() {
    
    // Construct data object
    const restartData = {
        action: "restart"
    };

    // Convert data to JSON string
    var jsonData = JSON.stringify(restartData);

    // Send data over WebSocket
    websocket.send(jsonData);

    // Optionally, you can handle response or feedback here
    // For example, update UI or show confirmation message
    console.log('New calibration factor sent:', restartData);
}

function downloadMeasurements() {
    const url = "/download"; // Endpoint to download the CSV file
  
    fetch(url)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.blob();
      })
      .then((blob) => {
        // Create a URL for the blob
        const blobUrl = URL.createObjectURL(blob);
  
        // Create a temporary link element
        const link = document.createElement("a");
        link.href = blobUrl;
        link.download = "measurements.csv";
  
        // Append link to the document and trigger click
        document.body.appendChild(link);
        link.click();
  
        // Clean up
        document.body.removeChild(link);
        URL.revokeObjectURL(blobUrl);
      })
      .catch((error) => {
        console.error("There was a problem with the fetch operation:", error);
        alert("Failed to download measurements.");
      });
  }
  