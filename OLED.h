#ifndef OLED_H
#define OLED_H

#ifdef ESP8266
  #include <Adafruit_SSD1306.h>

  // 128x64 OLED display constants
  #define OLED_RESET    0
  #define SCREEN_ADDRESS 0x3C
  #define SCREEN_WIDTH  128
  #define SCREEN_HEIGHT 64

  // 128x32 OLED display constants
  // #define OLED_RESET    0
  // #define SCREEN_ADDRESS 0x3C
  // #define SCREEN_WIDTH  128
  // #define SCREEN_HEIGHT 32

  // OLED I2C pins.
  // #define SDA_OLED D6
  // D2: default SDA pin for I2C in ESP8266.
  // D6 is GPIO14 on the ESP8266 12E.
  // #define SCL_OLED D5
  // D1: default SCL pin for I2C in ESP8266.
  // D5 is GPIO12 on the ESP8266 12E.

  // Declare display object
  extern Adafruit_SSD1306 oled;
  // extern TwoWire Wire2;

#elif defined(ESP32)
  #include "SPI.h"
  #include <Arduino_GFX_Library.h>

  // Define pins for display interface. You'll probably need to edit this for
  // your own needs:
  #define TFT_WIDTH  128
  #define TFT_HEIGHT 128

  // https://github.com/Bits4Bots/Lolin-S3-Mini-Pro?tab=readme-ov-file#function-pin
  // https://www.instructables.com/S3-Mini-Pro-Lolin-Projects/
  #define TFT_MOSI 38  // In some display driver board, it might be written as "SDA" and so on.
  #define TFT_SCLK 40
  #define TFT_CS   35  // Chip select control pin
  #define TFT_DC   36  // Data Command control pin
  #define TFT_RST  34  // Reset pin (could connect to Arduino RESET pin)
  #define TFT_BL   33  // LED back-light

  // Arduino GFX alternate library.
  #define GFX_BL TFT_BL // default backlight pin, you may replace DF_GFX_BL to actual backlight pin
  extern Arduino_GFX *gfx;
#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

// Function declaration
extern bool display_state;
extern int current_screen;
void clear_display();
void draw_display();
void setup_display();
void initDisplay();
void showLogo_regosh();
void draw_cabs(float absorbance, double abs_corr_factor);
void draw_abs(float absorbance);
void draw_trans(float transmittance, uint16_t intensity);
void draw_wifi_ip(String ip_address);
void draw_wifi_ssid(String ip_address);
void draw_blank();
void draw_error(String error_code);
void draw_button_indicators();
void draw_empty_red_dot();
void draw_filled_red_dot();
void display_off();
void display_on();
void display_toggle();
int display_next();
void draw_index(int index);
void draw_measurement(int index);

const unsigned char logo_regosh [] PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xe0, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0xfc, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0xff, 0xff, 0xff, 0x80, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xff, 0xff, 0xff, 0xe0, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0x0f, 0xff, 0xfc, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xff, 0xff, 0x07, 0xff, 0xfc, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0xff, 0xff, 0x07, 0xff, 0xf0, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x0e, 0x3f, 0xff, 0x0f, 0xff, 0xc3, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x40, 0x80, 0x00, 0x00, 0x00, 0x09, 0x87, 0xff, 0xff, 0xff, 0x18, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x80, 0x40, 0x00, 0x00, 0x00, 0x08, 0x31, 0xff, 0xff, 0xfc, 0x60, 0x80, 0x00, 0x00,
  0x00, 0x01, 0x00, 0x20, 0x00, 0x00, 0x00, 0x08, 0x0c, 0x7f, 0xff, 0xe1, 0x80, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0x08, 0x03, 0x1f, 0xff, 0x86, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x02, 0x1f, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0xc7, 0xfe, 0x30, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x02, 0x1f, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x18, 0xf8, 0xc0, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x02, 0x1f, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x06, 0x23, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x02, 0x1f, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x01, 0x98, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x06, 0x10, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x40, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x01, 0x00, 0x20, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x40, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x80, 0x40, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x40, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x60, 0x80, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x40, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x1e, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x40, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x00, 0x43, 0x00, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0x80, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0x80, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0x80, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x43, 0x80, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x00, 0x0c, 0x07, 0x00, 0x0c, 0x00, 0x0f, 0x00, 0x1e, 0x43, 0x8c, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x03, 0xbc, 0x3f, 0xc0, 0x3f, 0xb0, 0x3f, 0xc0, 0x7f, 0xc3, 0xbf, 0x00, 0x80, 0x00, 0x00,
  0x00, 0x03, 0xf4, 0x79, 0xe0, 0x71, 0xf0, 0x79, 0xe0, 0xf1, 0xc3, 0xf7, 0x80, 0x80, 0x00, 0x00,
  0x00, 0x03, 0xc0, 0xe0, 0x70, 0xe0, 0xf0, 0x70, 0x70, 0xc0, 0x43, 0xc1, 0xc0, 0x80, 0x00, 0x00,
  0x00, 0x03, 0x80, 0xc0, 0x30, 0xc0, 0x70, 0xe0, 0x30, 0xc0, 0x43, 0x81, 0xc0, 0x80, 0x00, 0x00,
  0x00, 0x03, 0x80, 0xc0, 0x30, 0xc0, 0x70, 0xc0, 0x38, 0xe0, 0x43, 0x80, 0xc0, 0x80, 0x00, 0x00,
  0x00, 0x03, 0x81, 0xff, 0xf1, 0xc0, 0x30, 0xc8, 0x38, 0xf8, 0x43, 0x80, 0xc0, 0x80, 0x00, 0x00,
  0x00, 0x03, 0x81, 0xff, 0xf1, 0xc0, 0x30, 0xcc, 0x38, 0x7f, 0xc3, 0x80, 0xc1, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x81, 0xc0, 0x01, 0xc0, 0x30, 0xc2, 0x38, 0x0f, 0xc3, 0x80, 0xc6, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x81, 0xc0, 0x00, 0xc0, 0x30, 0xc0, 0xf8, 0x01, 0xe3, 0x80, 0xd8, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x80, 0xc0, 0x00, 0xc0, 0x70, 0xe0, 0x78, 0x00, 0xe3, 0x80, 0xe0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x80, 0xe0, 0x00, 0xe0, 0xf0, 0xe0, 0x78, 0x00, 0xe3, 0x81, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x80, 0x70, 0x10, 0x71, 0xf0, 0x70, 0xf7, 0x81, 0xe3, 0x83, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x80, 0x3f, 0xf0, 0x3f, 0xb0, 0x3f, 0xe1, 0xff, 0xc3, 0x98, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x03, 0x00, 0x1f, 0xe0, 0x1e, 0x30, 0x1f, 0x80, 0xff, 0x43, 0x30, 0xc0, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x18, 0x40, 0x80, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x06, 0x43, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0xe0, 0x00, 0x00, 0x01, 0xd8, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0xc0, 0x00, 0x00, 0x00, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x19, 0x80, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x40, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x27, 0x40, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x27, 0x40, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x40, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x18, 0x80, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00
};

#endif
