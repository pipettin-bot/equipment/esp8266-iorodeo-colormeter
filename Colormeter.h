#ifndef COLORMETER_H
#define COLORMETER_H

#include <Arduino.h>
#include <Wire.h>
//#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include "src/Adafruit_TSL2591_Library/Adafruit_TSL2591.h"

extern Adafruit_TSL2591 tsl;
extern uint32_t luminosity;
extern bool is_reading;

void loop_sensor(void);
bool updateSensorReadings(void);
void updateBlanks();
void updateSensorConfig(const String& gain, const String& time);
void updateCalibrationConfig(const String& factor);
String serialize_state(void);
bool setup_sensor(void);
void restart_esp();

uint16_t simpleRead(void);
uint16_t advancedRead(void);
void unifiedSensorAPIRead(void);

#endif // COLORMETER_H
