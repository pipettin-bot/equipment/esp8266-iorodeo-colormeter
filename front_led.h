#ifndef FRONT_LED_H
#define FRONT_LED_H

#ifdef ESP32

// Choose library for driving the front LED.
#define USE_ADAFRUIT_NEOPIXEL
//#define USE_FASTLED

// Pin definitions
#define POWER_PIN 7   // Power pin for RGB LED (IO7)
#define RGB_LED_PIN 8   // Pin connected to the NeoPixel
#define NUM_LEDS 1   // Number of NeoPixels
#define BRIGHTNESS 24 // Max brightness (0-255)

// Function prototypes
void setup_blink(); // Initializes the RGB LED setup.
void setColor(int red, int green, int blue); // Sets the color of the NeoPixel.
void setBrightness(int brightness);
void tick_front_led();
void led_off();
void led_on();
void toggle_led();

#endif // ESP32

#endif // FRONT_LED_H
