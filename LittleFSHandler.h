#ifndef LITTLEFS_HANDLER_H
#define LITTLEFS_HANDLER_H

#include <Arduino.h>
#include <ArduinoJson.h>

extern int savedCount;

// Function declarations
void setup_defaults();
void initFS();

void writeBlankValues(JsonDocument& readings);
void readBlankValues(JsonDocument& blanks);

void writeWiFiConfig(JsonDocument& wifi_creds);
void readWiFiConfig(JsonDocument& wifi_creds);

void writeSensorConfig(JsonDocument& sensor_config);
void readSensorConfig(JsonDocument& sensor_config);

void writeCalibrationConfig(JsonDocument& calib_config);
void readCalibrationConfig(JsonDocument& calib_config);

bool recreateMeasurementFile();
void appendMeasurementToFile();
int countSavedMeasurements();

#define TSL2591_GAIN_DEFAULT "TSL2591_GAIN_HIGH"  // Increased from TSL2591_GAIN_MED
#define TSL2591_INTEGRATIONTIME_DEFAULT "TSL2591_INTEGRATIONTIME_300MS"

#endif  // LITTLEFS_HANDLER_H
