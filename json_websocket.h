#ifndef WEBSOCKET_H
#define WEBSOCKET_H

#include <ESPAsyncWebServer.h>

extern AsyncWebSocket websocket_1;
extern AsyncWebServer server;

void cleanupClients();
void setup_webserver();
void initWebSocket();
void notifyClients(String data);

#endif // WEBSOCKET_H
