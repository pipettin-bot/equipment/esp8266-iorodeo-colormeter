#include "front_led.h"




#ifdef USE_FASTLED

//#define FASTLED_ESP32_FLASH_LOCK 1
// See: https://github.com/FastLED/FastLED/issues/1220#issuecomment-829537474
//#define FASTLED_ESP32_I2S true // Does not compile.
// See: https://github.com/FastLED/FastLED/issues/1220#issuecomment-822677011
#include <FastLED.h>
// Docs: https://github.com/FastLED/FastLED/wiki/Controlling-leds

// Define the array of leds
CRGB leds[NUM_LEDS];

// Function to set the color of the NeoPixel
void setColor(int red, int green, int blue) {
  leds[0].setRGB(green, red, blue); // Set the first LED to red
  FastLED.show();  // Send the updated color to the hardware
}

// Function to set the brightness of the NeoPixel
void setBrightness(int brightness) {
  FastLED.setBrightness(brightness);
  FastLED.show();  // Send the updated color to the hardware
}

void setup_blink() {
  // Set the power pin to HIGH to power the RGB LED
  pinMode(POWER_PIN, OUTPUT);
  digitalWrite(POWER_PIN, HIGH);

  // ## Clockless types ##
  FastLED.addLeds<NEOPIXEL, RGB_LED_PIN>(leds, NUM_LEDS);  // GRB ordering is assumed
  FastLED.setBrightness(BRIGHTNESS);  // Set brightness level

  // White.
  setColor(255, 255, 255);
}




#elif defined(USE_ADAFRUIT_NEOPIXEL)

#include <Adafruit_NeoPixel.h>

Adafruit_NeoPixel pixels(NUM_LEDS, RGB_LED_PIN, NEO_GRB + NEO_KHZ800);

uint8_t r = 0;
uint8_t g = 0;
uint8_t b = 0;
uint8_t bright = 255;

void updateColor(){
  // Serial.printf("Color updated: R=%d, G=%d, B=%d, Brightness=%d\n", r, g, b, bright);
  uint8_t r_bright = bright * r / 255;
  uint8_t g_bright = bright * g / 255;
  uint8_t b_bright = bright * b / 255;
  // Serial.printf("Final values: R=%d, G=%d, B=%d\n", r_bright, g_bright, b_bright);
  // NOTE: Even though Adafruit's API says that Color accepts RGB, it seems to be GRB.
  //pixels.setPixelColor(0, pixels.Color(r_bright, g_bright, b_bright));
  pixels.setPixelColor(0, pixels.Color(g_bright, r_bright, b_bright));
  pixels.show();
}

// Function to set the color of the NeoPixel
void setColor(int red, int green, int blue) {
  r = red;
  g = green;
  b = blue;
  updateColor();
}

// Function to set the brightness of the NeoPixel
void setBrightness(int brightness) {
  bright = brightness;
  updateColor();
}

void setup_blink() {
  // Set the power pin to HIGH to power the RGB LED.
  pinMode(POWER_PIN, OUTPUT);
  digitalWrite(POWER_PIN, HIGH);

  // Initialize the NeoPixel strip object.
  pixels.begin();

  // White.
  setColor(255, 255, 255);
}

#endif






// Common functions.
#if defined(USE_FASTLED) || defined(USE_ADAFRUIT_NEOPIXEL)

unsigned long lastMillis = 0;  // Time of the last LED state change
bool ledState = false;         // Current state of the LED (on or off)
unsigned long led_on_time = 200;
unsigned long led_off_time = 1800;

void led_off(){
  // Serial.println("led off");
  ledState = false;
  // digitalWrite(POWER_PIN, LOW);
  setBrightness(0);
}

void led_on(){
  // Serial.println("led on");
  ledState = true;
  // digitalWrite(POWER_PIN, HIGH);
  setBrightness(BRIGHTNESS);
}

void toggle_led(){
  // Serial.println("toggle led");
  // Toggle the LED state
  ledState = !ledState;

  // Toggle the power.
  if(ledState){
    led_off();
  } else {
    led_on();
  }
  // digitalWrite(POWER_PIN, ledState ? HIGH : LOW);
  // setBrightness(ledState ? BRIGHTNESS : 0);
}

void tick_front_led() {
  // Check if it's time to change the LED state
  if ((millis() - lastMillis) >= (ledState ? led_on_time : led_off_time)) {
    // Toggle the LED state
    toggle_led();
    // Update the last time the function was executed
    lastMillis = millis();
  }
}

#endif