#ifdef ESP8266
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h> // Needed for ESPAsyncWebServer
#elif defined(ESP32)
  #include <WiFi.h>
  #include <AsyncTCP.h>  // Needed for ESPAsyncWebServer
#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

#include <ESPAsyncWebServer.h>
#include <ESPmDNS.h>

#include "WiFiSetup.h"

// Get: restart_esp
#include "Colormeter.h"
// Get: writeWiFiConfig
#include "LittleFSHandler.h"
// Get: wifi_creds data object
#include "shared_objects.h"

// Replace with the AP's or your network credentials.
// NOTE: "default_ssid" seems to be reserved by some library. Use "ssid_default" instead.
const char* ssid_default = "Colormeter595-C2";
const char* password_default = "password";
const char* host = "color-c2";

void updateWiFiConfig(const String& ssid, const String& password) {
  Serial.print("Updating WiFi config with SSID '");
  Serial.print(ssid);
  Serial.print("' and password '");
  Serial.print(password);
  Serial.println("'.");

  // Save and write configuration.
  wifi_creds["ssid"] = ssid;
  wifi_creds["password"] = password;
  writeWiFiConfig(wifi_creds);

  restart_esp();
}

void clearWiFiConfig(){
  // Clear previous configuration.
  // https://github.com/esp8266/Arduino/issues/119#issuecomment-157418957

  // This method with the true parameter will also erase saved credentials in flash, 
  // which could help prevent accidental reconnection attempts from interfering with AP mode.
  WiFi.disconnect(true);

  WiFi.softAPdisconnect();
}

bool is_ap = false;

String get_ip(){
  if (is_ap) {
    return WiFi.softAPIP().toString();
  } else {
    return WiFi.localIP().toString();
  }
}

String get_net(){
  if (is_ap) {
    return String(ssid_default);
  } else {
    return WiFi.SSID(); // Already a String.
  }
}

void initAP(){
  is_ap = true;

  // Ensure we are in AP mode.
  WiFi.mode(WIFI_AP);

  // Setup WiFi as an Access Point (a.k.a. hotspot or AP).
  Serial.print("Configuring access point with SSID '");
  Serial.print(ssid_default);
  Serial.print("' and PASS '");
  Serial.print(password_default);
  Serial.println("'.");
  
  // Initialize the soft AP with the desired credentials.
  // You can remove the password parameter if you want the AP to be open.
  if (WiFi.softAP(ssid_default, password_default)) {
    Serial.println("Access Point setup successful.");
  } else {
    Serial.println("Failed to setup Access Point.");
  }
  
  // Get the IP.
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("Access Point setup with IP address: ");
  Serial.println(myIP);
}

// Initialize WiFi
void initWiFiDefaults(JsonDocument& wifi_creds) {
  if(wifi_creds.isNull()){
    wifi_creds["ssid"] = ssid_default;
    wifi_creds["password"] = password_default;
  }
}

void initWiFi(JsonDocument& wifi_creds) {

  initWiFiDefaults(wifi_creds);

  const char* ssid = wifi_creds["ssid"];
  const char* password = wifi_creds["password"];

  Serial.print("Connecting to WiFi network with SSID: '");
  Serial.print(ssid);
  Serial.println("'.");

  // Clear previous configuration.
  // WiFi.disconnect();
  // WiFi.softAPdisconnect();

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  unsigned long startAttemptTime = millis();
  
  Serial.print("Connecting to WiFi ..");
  // Try to connect.
  while ((WiFi.status() != WL_CONNECTED) && (millis() - startAttemptTime < 10000)) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println();

  if (WiFi.status() == WL_CONNECTED) {
    Serial.print("Connected to WiFi network '");
    Serial.print(ssid);
    Serial.print("' with IP: ");
    Serial.println(WiFi.localIP());
  } else {
    Serial.println("Failed to connect. Switching to AP mode.");
    // Clear previous configuration.
    clearWiFiConfig();
    // Create AP.
    initAP();
  }

  /* Use mDNS for host name resolution*/
  if (!MDNS.begin(host)) { //http://esp32.local
    Serial.println("Error setting up MDNS responder!");
  }
  Serial.println("mDNS responder started");
}
