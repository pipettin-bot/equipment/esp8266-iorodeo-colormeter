#include <Wire.h>
//#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
// Use of the "src" subfolder.
// https://arduino.github.io/arduino-cli/1.2/sketch-specification/#src-subfolder
// https://forum.arduino.cc/t/arduino-ide-cant-find-local-custom-libraries-in-relative-path/669421/8
#include "src/Adafruit_TSL2591_Library/Adafruit_TSL2591.h"

// Include main header.
#include "Colormeter.h"
// Filesystem tools
#include "LittleFSHandler.h"
// Get: data objects
#include "shared_objects.h"
// Builtin LED stuff.
#include "front_led.h"

Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)

void restart_esp(){
  // Restart the ESP8266.
  // ESP.reset() is a hard reset and can leave some of the registers in the old state which can lead to problems, its more or less like the reset button on the PC.
  // ESP.restart() tells the SDK to reboot, so its a more clean reboot, use this one if possible.
  // Source: https://www.pieterverhees.nl/prototyping-builds/esp8266/130-difference-between-esp-reset-and-esp-restart
  ESP.restart();
}

void updateBlanks(){
  // Update blank values from current readings.

  Serial.println("Updating blanks...");
  // Save current readings as blank JSON data to a persistent file.
  writeBlankValues(readings);
  // Read the blank values from the JSON file.
  readBlankValues(blanks);
  Serial.println("Blank data saved to config file and applied.");
}

void updateCalibrationConfig(const String& factor){
  float factor_numeric = factor.toFloat();
  calib_config["factor"] = factor_numeric;
  writeCalibrationConfig(calib_config);
}

String serialize_state(void)
{
  // Create a temporary buffer for JSON serialization.
  JsonDocument tempDoc;
  
  // Copy 'readings' to 'tempDoc' to avoid modifying the original.
  tempDoc = readings;

  // Add gain and time information.
  tempDoc["current_gain"] = sensor_config["gain"];
  tempDoc["current_time"] = sensor_config["time"];

  // Add calibration factor.
  tempDoc["adj_factor"] = calib_config["factor"];

  // Create a String to hold the JSON output.
  String jsonString;
  // Serialize JSON to String.
  serializeJson(tempDoc, jsonString);

  return jsonString;
}


void updateSensorConfig(const String& gain, const String& time) {

    // Process the gain and time values as needed
    Serial.print("Received gain: ");
    Serial.println(gain);
    Serial.print("Received integration time: ");
    Serial.println(time);

    // Save config.
    bool update_gain = true;
    bool update_time = true;

    // Set gain based on selected value
    if (gain == "TSL2591_GAIN_LOW") {
        tsl.setGain(TSL2591_GAIN_LOW);
        Serial.println("Setting sensor gain to Low (1x)");
    } else if (gain == "TSL2591_GAIN_MED") {
        tsl.setGain(TSL2591_GAIN_MED);
        Serial.println("Setting sensor gain to Medium (25x)");
    } else if (gain == "TSL2591_GAIN_HIGH") {
        tsl.setGain(TSL2591_GAIN_HIGH);
        Serial.println("Setting sensor gain to High (428x)");
    } else if (gain == "TSL2591_GAIN_MAX") {
        tsl.setGain(TSL2591_GAIN_MAX);
        Serial.println("Setting sensor gain to Max (9876x)");
    } else {
        Serial.println("Unknown gain value received");
        // Handle unknown gain value
        update_time = false;
    }

    // Set integration time based on selected value
    if (time == "TSL2591_INTEGRATIONTIME_100MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);
        Serial.println("Setting integration time to 100ms");
    } else if (time == "TSL2591_INTEGRATIONTIME_200MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
        Serial.println("Setting integration time to 200ms");
    } else if (time == "TSL2591_INTEGRATIONTIME_300MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
        Serial.println("Setting integration time to 300ms");
    } else if (time == "TSL2591_INTEGRATIONTIME_400MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
        Serial.println("Setting integration time to 400ms");
    } else if (time == "TSL2591_INTEGRATIONTIME_500MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
        Serial.println("Setting integration time to 500ms");
    } else if (time == "TSL2591_INTEGRATIONTIME_600MS") {
        tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);
        Serial.println("Setting integration time to 600ms");
    } else {
        Serial.println("Unknown integration time value received");
        // Handle unknown integration time value
        update_gain = false;
    }

    Serial.print("Initial sensor configuration: ");
    serializeJson(sensor_config, Serial);
    Serial.println();

    // Only apply updates if the values are valid.
    String new_gain;
    if(update_time){
      new_gain = gain;
      sensor_config["gain"] = new_gain;
    }
    String new_time;
    if(update_gain){
      new_time = time;
      sensor_config["time"] = new_time;
    }

    Serial.print("Final sensor configuration: ");
    serializeJson(sensor_config, Serial);
    Serial.println();

    // Save configuration to persistent storage.
    writeSensorConfig(sensor_config);
}

/**************************************************************************/
/*
    Displays some basic information on this sensor from the unified
    sensor API sensor_t type (see Adafruit_Sensor for more information)
*/
/**************************************************************************/
void printSensorDetails(void)
{
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.print  (F("Sensor:       ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:   ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:    ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:    ")); Serial.print(sensor.max_value); Serial.println(F(" lux"));
  Serial.print  (F("Min Value:    ")); Serial.print(sensor.min_value); Serial.println(F(" lux"));
  Serial.print  (F("Resolution:   ")); Serial.print(sensor.resolution, 4); Serial.println(F(" lux"));
  Serial.println(F("------------------------------------"));
  Serial.println(F(""));
}

// Sensor read variables.
uint32_t luminosity = 0;
bool is_reading = false;
bool new_data = true;

#ifdef ESP8266

// Regular read for ESP8266 (single-core).
// TODO: Actually FreeRTOS also works on the ESP8266. 
uint32_t getFullLuminosity(){
  // In this case, leave "new_data" as true.
  // The data returned by this function is always new.
  is_reading = true;
  luminosity = tsl.getFullLuminosity();
  is_reading = false;
  return luminosity;
}

#elif defined(ESP32)

// Read from sensor as a non-blocking task on an ESP32 (dual-core).
// https://github.com/adafruit/Adafruit_TSL2591_Library/blob/master/Adafruit_TSL2591.cpp#L300
void sensorTaskFreeRTOS(void *pvParameters) {
  while (1) {
      // Flag the read.
      is_reading = true;
      led_off();

      // Enable the sensor (draws power).
      tsl.enable();
      
      // Non-blocking delay to wait for the sensor read.
      for (uint8_t d = 0; d <= tsl.getTiming(); d++) {
        vTaskDelay(120 / portTICK_PERIOD_MS);
      }
      
      // The sensor should have produced data by now. Try reading it.
      uint16_t y = tsl.read16(TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN0_LOW);
      uint32_t x = tsl.read16(TSL2591_COMMAND_BIT | TSL2591_REGISTER_CHAN1_LOW);

      // Make the full luminosity output value.
      x <<= 16;
      x |= y;
      
      // Disable for low power draw.
      tsl.disable();

      // Save the value.
      luminosity = x;
      new_data = true;
      Serial.printf("Luminosity: %lu\n", luminosity);

      // No longer reading.
      is_reading = false;
      led_on();
      
      // Delay briefly until the next read.
      vTaskDelay(100 / portTICK_PERIOD_MS);
  }
}
uint32_t getFullLuminosity(){
  new_data = false;
  return luminosity;
}
void setup_rtos_task(){
  xTaskCreatePinnedToCore(
      sensorTaskFreeRTOS,   // Task function
      "SensorTask",         // Name for the task
      4096,     // Stack size (in bytes)
      NULL,     // Parameters (not used)
      1,        // Priority (higher = more important)
      NULL,     // Task handle (not needed here)
      1         // Run on Core 1
  );
}
#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

void updateRawReadings(){
  // Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum.
  uint32_t lum = getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;

  readings["full_lum"] = full;
  readings["ir_lum"] =  ir;
  readings["vis_lum"] = full - ir;
  readings["lux_lum"] = tsl.calculateLux(full, ir);

  // Use current as default if no blanks are available yet.
  if (blanks.isNull()){
    blanks["full_lum"] = readings["full_lum"];
    blanks["ir_lum"] = readings["ir_lum"];
    blanks["vis_lum"] = readings["vis_lum"];
    blanks["lux_lum"] = readings["lux_lum"];
  }
}

void updateMeasurements(){
  // Check if reading blank values was successful
  if (blanks.isNull()) {
    // Set derived measurements to null.
    readings["full_trans"] = (char*)NULL;
    readings["ir_trans"] = (char*)NULL;
    readings["vis_trans"] = (char*)NULL;
    readings["full_abs"] = (char*)NULL;
    readings["ir_abs"] = (char*)NULL;
    readings["vis_abs"] = (char*)NULL;
  } else {
    // Nice message.
    //Serial.print("Loaded blank values from storage: ");
    //serializeJson(blanks, Serial);
    //Serial.println();

    // Calculate transmitance: T = I/I0
    readings["full_trans"] = readings["full_lum"].as<float>() / blanks["full_lum"].as<float>();
    readings["ir_trans"] = readings["ir_lum"].as<float>() / blanks["ir_lum"].as<float>();
    readings["vis_trans"] = readings["vis_lum"].as<float>() / blanks["vis_lum"].as<float>();

    // Get absorbance correction factor.
    double abs_corr_factor = calib_config["factor"].as<double>();
    
    // Calculate absorbance: A = log10(1/T) = -log10(T) = -log10(I/I0) = log10(I0/I)
    readings["full_abs"] = abs_corr_factor * -log10(readings["full_trans"].as<double>());
    readings["ir_abs"] = abs_corr_factor * -log10(readings["ir_trans"].as<double>());
    readings["vis_abs"] = abs_corr_factor * -log10(readings["vis_trans"].as<double>());
  }
}

bool updateSensorReadings(void)
{
  if(new_data){
    // Get new readings and update the global readings object.
    updateRawReadings();
    // Re compute measurements using blanks.
    updateMeasurements();
    return true;
  }

  // Do nothing if there is no new data.
  return false;
}


void display_sensor_setup(){
  /* Display the gain and integration time for reference sake */
  Serial.println(F("------------------------------------"));
  Serial.print  (F("Gain:         "));
  tsl2591Gain_t gain = tsl.getGain();
  switch(gain)
  {
    case TSL2591_GAIN_LOW:
      Serial.println(F("1x (Low)"));
      break;
    case TSL2591_GAIN_MED:
      Serial.println(F("25x (Medium)"));
      break;
    case TSL2591_GAIN_HIGH:
      Serial.println(F("428x (High)"));
      break;
    case TSL2591_GAIN_MAX:
      Serial.println(F("9876x (Max)"));
      break;
  }
  Serial.print  (F("Timing:       "));
  Serial.print((tsl.getTiming() + 1) * 100, DEC);
  Serial.println(F(" ms"));
  Serial.println(F("------------------------------------"));
  Serial.println(F(""));
}


/**************************************************************************/
/*
    Configures the gain and integration time for the TSL2591
*/
/**************************************************************************/
void configureSensor(void){
  // Set defaults.
  if(sensor_config.isNull()){
    // Set some defaults if null.
    sensor_config["gain"] = TSL2591_GAIN_DEFAULT;
    sensor_config["time"] = TSL2591_INTEGRATIONTIME_DEFAULT;
    // Save configuration to persistent storage.
    writeSensorConfig(sensor_config);
  }

  // Apply configuration.
  String gain = sensor_config["gain"];
  String time = sensor_config["time"];
  Serial.print("Setting sensor configuration to: ");
  serializeJson(sensor_config, Serial);
  Serial.println();
  updateSensorConfig(gain, time);

  // Display information.
  display_sensor_setup();
}

bool setup_sensor(void)
{
  Serial.println(F("Starting Adafruit TSL2591 Test!"));

  // Initiate the sensor connection with the begin method.
  // Note that it can accept a "Wire" object as explained here:
  //   https://adafruit.github.io/Adafruit_TSL2591_Library/html/class_adafruit___t_s_l2591.html#a6505cfde541782908345721eac2a4eeb
  if (tsl.begin(&Wire)){
    Serial.println(F("Found a TSL2591 sensor"));
  } else {
    Serial.println(F("No sensor found ... check your wiring?"));
    return false;
  }

  #ifdef ESP32
  setup_rtos_task();
  #endif

  /* Display some basic information on this sensor */
  printSensorDetails();

  /* Configure the sensor */
  configureSensor();

  /* Make initial measurements and generate default blanks */
  updateSensorReadings();

  // Now we're ready to get readings ... move on to loop()!
  return true;
}


/**************************************************************************/
/*
    Shows how to perform a basic read on visible, full spectrum or
    infrared light (returns raw 16-bit ADC values)
*/
/**************************************************************************/
uint16_t simpleRead(void)
{
  // Simple data read example. Just read the infrared, fullspecrtrum diode
  // or 'visible' (difference between the two) channels.
  // This can take 100-600 milliseconds! Uncomment whichever of the following you want to read
  uint16_t x = tsl.getLuminosity(TSL2591_VISIBLE);
  //uint16_t x = tsl.getLuminosity(TSL2591_FULLSPECTRUM);
  //uint16_t x = tsl.getLuminosity(TSL2591_INFRARED);

  Serial.print(F("[ ")); Serial.print(millis()); Serial.print(F(" ms ] "));
  Serial.print(F("Luminosity: "));
  Serial.println(x, DEC);

  return x;
}

/**************************************************************************/
/*
    Show how to read IR and Full Spectrum at once and convert to lux
*/
/**************************************************************************/
uint16_t advancedRead(void)
{
  // More advanced data read example. Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum
  // That way you can do whatever math and comparisons you want!
  uint32_t lum = tsl.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  Serial.print(F("[ ")); Serial.print(millis()); Serial.print(F(" ms ] "));
  Serial.print(F("IR: ")); Serial.print(ir);  Serial.print(F("  "));
  Serial.print(F("Full: ")); Serial.print(full); Serial.print(F("  "));
  Serial.print(F("Visible: ")); Serial.print(full - ir); Serial.print(F("  "));
  Serial.print(F("Lux: ")); Serial.println(tsl.calculateLux(full, ir), 6);

  return full;
}

/**************************************************************************/
/*
    Performs a read using the Adafruit Unified Sensor API.
*/
/**************************************************************************/
void unifiedSensorAPIRead(void)
{
  /* Get a new sensor event */
  sensors_event_t event;
  tsl.getEvent(&event);

  /* Display the results (light is measured in lux) */
  Serial.print(F("[ ")); Serial.print(event.timestamp); Serial.print(F(" ms ] "));
  if ((event.light == 0) |
      (event.light > 4294966000.0) |
      (event.light <-4294966000.0))
  {
    /* If event.light = 0 lux the sensor is probably saturated */
    /* and no reliable data could be generated! */
    /* if event.light is +/- 4294967040 there was a float over/underflow */
    Serial.println(F("Invalid data (adjust gain or timing)"));
  }
  else
  {
    Serial.print(event.light); Serial.println(F(" lux"));
  }
}

void loop_sensor(void){
  updateSensorReadings();
}