#ifndef JSON_RPC_H
#define JSON_RPC_H

void parse_json_rpc();
void send_alive();
void echo_serial();
void log(String message, String level);
void log_info(String message);

#endif // JSON_RPC_H
