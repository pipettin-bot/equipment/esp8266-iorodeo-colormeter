#ifndef FRONT_LED_H
#define FRONT_LED_H

// Function prototypes
void setup_blink(); // Initializes the RGB LED setup.
void setColor(int red, int green, int blue); // Sets the color of the NeoPixel.

#endif // FRONT_LED_H
