
#include "front_led.h"

void setup() { 
  setup_blink();
}

void loop() { 
  // Turn the LED on, then pause
  setColor(0, 255, 0); // Set the first LED to red
  delay(500);
  // Now turn the LED off, then pause
  setColor(0, 0, 0);
  delay(500);
}
