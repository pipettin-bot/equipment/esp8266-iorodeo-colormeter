#include "front_led.h"

#include <FastLED.h>
// Docs: https://github.com/FastLED/FastLED/wiki/Controlling-leds

// Define the array of leds
CRGB leds[1];

// Function to set the color of the NeoPixel
void setColor(int red, int green, int blue) {
  leds[0].setRGB(green, red, blue); // Set the first LED to red
  FastLED.show();  // Send the updated color to the hardware
}

void setup_blink() {
  // Set the power pin to HIGH to power the RGB LED
  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH);

  // ## Clockless types ##
  FastLED.addLeds<NEOPIXEL, 8>(leds, 1);  // GRB ordering is assumed
  FastLED.setBrightness(50);  // Set brightness level

  // 595 nm
  setColor(255, 207, 0);
}
