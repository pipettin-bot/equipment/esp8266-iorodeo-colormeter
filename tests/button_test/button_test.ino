const int BUTTON_PIN_RIGHT = 48; // Define the pin connected to the button
const int DEBOUNCE_DELAY = 50;  // Debounce delay in milliseconds

void setup() {
  Serial.begin(115200);                  // Initialize Serial Monitor
  pinMode(BUTTON_PIN_RIGHT, INPUT_PULLUP); // Configure the pin with internal pull-up
  Serial.println("Button test started. Press the button to see the output.");
}

void loop() {
  static int lastButtonState = HIGH; // Store the last button state
  static unsigned long lastDebounceTime = 0; // For debouncing

  int currentButtonState = digitalRead(BUTTON_PIN_RIGHT); // Read the button state

  // Debouncing logic
  if (currentButtonState != lastButtonState) {
    lastDebounceTime = millis(); // Reset debounce timer
  }

  if ((millis() - lastDebounceTime) > DEBOUNCE_DELAY) {
    // If the button state has stabilized
    if (currentButtonState == LOW) {
      Serial.println("Button pressed!");
    } else {
      Serial.println("Button released!");
    }
  }

  lastButtonState = currentButtonState; // Update the last button state
}
