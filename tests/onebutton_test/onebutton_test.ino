
#include "OneButton.h"

// Example pin assignments for a ESP32 board
#define PIN_INPUT GPIO_NUM_48

// Setup a new OneButton on pin PIN_INPUT
OneButton button = OneButton(PIN_INPUT, true, true);

void setup()
{
  Serial.begin(115200);
  Serial.println("One Button Example with polling.");
  button.attachClick(click);
} // setup

void loop()
{
  button.tick();
  delay(10);
} // loop

void click()
{
  Serial.println("Button clicked!");
} // click
