#include <Adafruit_NeoPixel.h>

#define POWER_PIN   7  // Power pin for RGB LED (IO7)
#define PIN         8  // Pin connected to the NeoPixel
#define NUMPIXELS   1  // Number of NeoPixels
#define BRIGHTNESS  150  // Max brightness (0-255)

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

int redValue = 255;   // Initial red value
int greenValue = 0;   // Initial green value
int blueValue = 0;    // Initial blue value

int transitionSpeed = 5;  // Adjust this value to make transitions faster or slower

void setup() {
  // Set the power pin to HIGH to power the RGB LED
  pinMode(POWER_PIN, OUTPUT);
  digitalWrite(POWER_PIN, HIGH);

  Serial.begin(9600);
  Serial.println("Hola!");

  pixels.begin();        // Initialize NeoPixel
  pixels.setBrightness(BRIGHTNESS);  // Set brightness level
}

void loop() {
  Serial.println("Loop!");

  // Gradually transition from red to green
  for (greenValue = 0; greenValue <= 255; greenValue += transitionSpeed) {
    redValue -= transitionSpeed;  // Decrease red value
    setColor(redValue, greenValue, blueValue);
    delay(10);  // Adjust delay for smoother/faster transitions
  }

  // Gradually transition from green to blue
  for (blueValue = 0; blueValue <= 255; blueValue += transitionSpeed) {
    greenValue -= transitionSpeed;  // Decrease green value
    setColor(redValue, greenValue, blueValue);
    delay(10);
  }

  // Gradually transition from blue to red
  for (redValue = 0; redValue <= 255; redValue += transitionSpeed) {
    blueValue -= transitionSpeed;  // Decrease blue value
    setColor(redValue, greenValue, blueValue);
    delay(10);
  }
}

// Function to set the color of the NeoPixel
void setColor(int red, int green, int blue) {
  pixels.setPixelColor(0, pixels.Color(red, green, blue));
  pixels.show();  // Send the updated color to the hardware
}
