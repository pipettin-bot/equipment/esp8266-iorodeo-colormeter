#include "OLED.h"
#include "WiFiSetup.h"

// Set MAX_SCREEN to the max "current_screen".
// This value is used to wrap-around the screen selection feature.
#define MAX_SCREEN 4

// For the info display.
#define LED_MODEL "595 nm"

#ifdef ESP8266

#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

/* Initialize the display object
See: https://adafruit.github.io/Adafruit_SSD1306/html/class_adafruit___s_s_d1306.html#a650aa88a18f2d6a5c2d5442602d12286
  w: Display width in pixels
  h: Display height in pixels
  twi: Pointer to an existing TwoWire instance (e.g. &Wire, the micro-controller's primary I2C bus).
  rst_pin: Reset pin (using Arduino pin numbering), or -1 if not used (some displays might be wired to share the micro-controller's reset pin).
*/

// Use the main I2C interface (i.e. pass "&Wire" here).
Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// Use a second I2C interface, you need to define "Wire2" and use it in place of "Wire".
// Note that this is actually impossible in the ESP8266. Even if a second "Wire instance" is used,
// only the last call "begin()" will count.
// TwoWire Wire2 = TwoWire();
// Adafruit_SSD1306 oled(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire2, OLED_RESET);

void clear_display(){
  oled.clearDisplay();
}

void draw_display(){
  oled.display();
}

void initDisplay() {
  // If we passed the "Wire2" I2C interface to the Adafruit_SSD1306 constructor above,
  // then we must set the pins of that same interface here.
  // Wire2.begin(SDA_OLED, SCL_OLED);

  if(!oled.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed, please restart."));
    for(;;); // Don't proceed, loop forever
  } else {
    Serial.println(F("SSD1306 allocation succeeded."));
  }
  clear_display();
  draw_display();
}

void showLogo_regosh() {
    clear_display(); // Make sure the display is cleared
    // Note: 'logo_regosh' needs to be declared in the main file or included here
    extern const uint8_t logo_regosh[];
    oled.drawBitmap(0, 0, logo_regosh, 128, 64, WHITE);
    oled.setTextSize(2);
    oled.setTextColor(WHITE);
    draw_display();
}

void setup_display(){
  Serial.println("=========== Initializing OLED Screen ===========");

  // Init display.
  initDisplay();
  delay(200);
  // Draw logo.
  showLogo_regosh();
  delay(1000);
  // Clear logo.
  clear_display();
  draw_display();

  Serial.println("=========== OLED Screen Initialized ===========");
}

void draw_qty(char* qty_name, float qty_value){
  oled.setTextSize(2);
  oled.setTextColor(WHITE);
  oled.setCursor(40, 20);
  oled.print(qty_name);
  oled.println(":");
  oled.setCursor(40, 45);
  oled.println(qty_value);
}

void draw_blank(){
  clear_display();
  oled.setTextSize(2);
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println("Blank data saved");
  // Update the display
  draw_display();
  delay(30);
}

void draw_wifi_ip(String ip_address){
  oled.setTextSize(1);
  oled.setTextColor(WHITE);
  oled.setCursor(0,0);
  oled.println(ip_address);
}

void draw_wifi_ssid(String ssid){
  // TODO: Not implemented.
}

void draw_error(String error_code) {
  clear_display();
  oled.setTextSize(2);
  oled.setTextColor(WHITE);
  // Error notice.
  oled.setCursor(40, 20);
  oled.println("ERROR:");
  // Error code.
  oled.setCursor(40, 45);
  oled.println(error_code);
  draw_display();
}

#elif defined(ESP32)

#include <Arduino_GFX_Library.h>
#include "front_led.h"

// Software SPI
/* More data bus class: https://github.com/moononournation/Arduino_GFX/wiki/Data-Bus-Class */
// Arduino_DataBus *bus = new Arduino_SWSPI(TFT_DC /* DC */, TFT_CS /* CS */, TFT_SCLK /* SCK */, TFT_MOSI /* MOSI */, GFX_NOT_DEFINED /* MISO */);
Arduino_DataBus *bus = new Arduino_HWSPI(TFT_DC, TFT_CS, TFT_SCLK, TFT_MOSI);
// Arduino_DataBus *bus = create_default_Arduino_DataBus();

/* More display class: https://github.com/moononournation/Arduino_GFX/wiki/Display-Class */
Arduino_GFX *gfx = new Arduino_GC9107(bus, TFT_RST, 0 /* rotation */, true /* IPS */);
// Also works, but needs a different rotation in initDisplay (below), and some of the screen is cropped top-side.
// Arduino_GFX *gfx = new Arduino_GC9A01(bus, TFT_RST, 2 /* rotation */, true /* IPS */);

// In theory one could use an adafruit library, but i've had no luck.
//   https://github.com/Bodmer/TFT_eSPI/discussions/1922#discussioncomment-3786890
//   https://github.com/Bits4Bots/Lolin-S3-Mini-Pro/blob/main/Tetris/Tetris.ino
//Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);


void clear_display(){
  // gfx->startWrite();  // Double Buffering, start writing.
  gfx->fillScreen(BLACK);  // Clear the screen
}

void draw_display(){
  // Nada.
  // gfx->endWrite();  //Double Buffering: Commit the framebuffer to the screen
}

bool display_state = false;

void initDisplay() {
  // Init TFT display.
  Serial.println(F("Starting display."));

  // Initialize SPI with a custom frequency (e.g., 40 MHz). Does not work, blank screen.
  // SPI.begin();
  // SPI.begin(TFT_SCLK, GFX_NOT_DEFINED, TFT_MOSI, TFT_CS); // Initialize SPI pins
  // SPI.beginTransaction(SPISettings(20000000, MSBFIRST, SPI_MODE0)); // Set SPI clock to 40 MHz
  // SPI.setFrequency(20000000);

  if(!gfx->begin()){
    Serial.println("Display initialization failed!");
  } else {
    // Backlight on
    pinMode(GFX_BL, OUTPUT); // TFT_BL pin.
    display_state = false;
    display_on();

    // Set the correct orientation (avoid mirroring).
    gfx->setRotation(2);  // Arduino_GC9107
    // gfx->setRotation(0);  // Arduino_GC9A01

    // Set the correct colors. Also settable throug IPS=true in Arduino_GC9107.
    // gfx->invertDisplay(true);

    clear_display();
    draw_display();

    Serial.println(F("Display started!"));
  }
}

int current_screen = 1;
int display_next(){
  // Switch to next screen.
  //  0: Turn display off.
  //  1: Show absorbance.
  //  2: Show transmittance.
  current_screen++;

  // Turn off display if on, and if state must wrap to off state.
  if(current_screen > MAX_SCREEN){
    current_screen = 0;
    if(display_state){
      display_off();
    }
    // Early return for the now off display.
    return current_screen;
  }
  
  // Turn on display (if off).
  if(!display_state){
    display_on();
  }

  return current_screen;
}

void display_toggle(){
  if(display_state){
    display_off();
  } else{
    display_on();
  }
}

void display_off(){
  // Serial.println("Display off");
  if(display_state){
    display_state = false;
    // TFT_BL off.
    digitalWrite(GFX_BL, LOW);
    // Set front led color to RED.
    setColor(255, 0, 0);
  }
}

void display_on(){
  // Serial.println("Display on");
  if(!display_state){
    display_state = true;
    // TFT_BL on.
    digitalWrite(GFX_BL, HIGH);
    // Set front led color to "595 nm".
    setColor(255, 207, 0);
    // Default to first screen if needed.
    if(current_screen == 0){
      clear_display();
      current_screen = 1;
    }
  }
}

void showLogo_regosh() {
    clear_display();

    // Note: 'logo_regosh' needs to be declared in the main file or included here
    extern const uint8_t logo_regosh[];
    gfx->drawBitmap(0, 24, logo_regosh, 128, 64, WHITE);  // Adjust bitmap size and colors for TFT

    // Branding
    gfx->setTextSize(1);
    gfx->setTextColor(WHITE);
    gfx->setCursor(0, 100);
    gfx->print("pin29's ODi -> ");
    gfx->setTextColor(YELLOW);
    gfx->println(LED_MODEL);

    // Set text size and color for the TFT screen
    gfx->setTextSize(2);
    gfx->setTextColor(WHITE);
    draw_display();
}

void setup_display(){
  Serial.println("=========== Initializing TFT Screen ===========");

  // Init display.
  initDisplay();
  delay(200);
  // Draw logo.
  showLogo_regosh();
  delay(1000);
  // // Clear logo.
  // clear_display();
  // draw_display();

  Serial.println("=========== TFT Screen Initialized ===========");
}

String format_qty(float qty_value, int places, int txt_width){
  // Format decimal places.
  char buffer[10];
  dtostrf(qty_value, txt_width, places, buffer);  // 6 is the minimum width, 3 is the number of decimal places
  String qty_str = String(buffer);    // Convert the char buffer to a String
  // String qty_str = String(qty_value, 3);

  return qty_str;
}

#define OFFSET 13

void draw_qty(char * qty_name, float qty_value){

  // Use width 6 to account for negative signs.
  String qty_str = format_qty(qty_value, 3, 6);

  // Quantity name.
  gfx->setTextSize(3);
  gfx->setCursor(20, 40-OFFSET);
  gfx->setTextColor(0x8c71);  // Grayish
  gfx->print(qty_name);
  gfx->println(":");

  // Quantity value.
  // Use X=2 to account for negative signs
  gfx->setCursor(2, 65-OFFSET);
  gfx->setTextColor(WHITE);
  gfx->println(qty_str);
}

void draw_factor(float qty_value){

  String qty_str = format_qty(qty_value, 2, 4);

  gfx->setTextSize(1);
  gfx->setTextColor(0x8c71);  // Grayish
  gfx->setCursor(40, 80);
  gfx->print("C = ");
  gfx->print(qty_str);
  gfx->println("");
}

void draw_intensity(uint16_t intensity){

  String qty_str = String(intensity);

  gfx->setTextSize(1);
  gfx->setTextColor(0x8c71);  // Grayish
  gfx->setCursor(40, 80);
  gfx->print("I = ");
  gfx->print(qty_str);
}

void draw_blank(){
  // gfx->fillScreen(RED);  // Fill with red.
  clear_display();
  gfx->setTextSize(2);
  gfx->setTextColor(WHITE);
  gfx->setCursor(10, 50);
  gfx->println("Blank set");
  gfx->setCursor(50, 70);
  gfx->println(":O");
}

void draw_wifi_ip(String ip_address) {
  gfx->setTextSize(1);
  gfx->setTextColor(GREEN);
  gfx->setCursor(0, 0);
  // gfx->print("IP: ");
  gfx->println(ip_address);
}

void draw_wifi_ssid(String ssid){
  gfx->setTextSize(1);
  gfx->setTextColor(GREEN);
  gfx->setCursor(0, 10);
  gfx->println(ssid);
}

void draw_error(String error_code) {
  clear_display();
  gfx->setTextSize(2);
  gfx->setTextColor(RED);
  // Error notice.
  gfx->setCursor(40, 20);
  gfx->println("ERROR:");
  // Error code.
  gfx->setCursor(40, 45);
  gfx->println(error_code);
  draw_display();
}

String format_int_qty(int qty_value, int zero_pad){
  // Create format string dynamically
  char format_str[10];
  sprintf(format_str, "%%0%dd", zero_pad);

  // Format the integer
  char buffer[10];
  sprintf(buffer, format_str, qty_value);

  return String(buffer);
}

void draw_index(int index){
  String formatted_index = format_int_qty(index, 2);
  gfx->setTextSize(2);
  gfx->setCursor(20+10, 100-5); // Button labels are at Y=120, use Y=100.
  gfx->setTextColor(0x8c71);
  gfx->print("ID: ");
  gfx->setTextColor(YELLOW);
  gfx->print(formatted_index);
}

void draw_measurement(int index){
  // gfx->fillScreen(RED);  // Fill with red.
  clear_display();
  gfx->setTextSize(2);
  gfx->setTextColor(WHITE);
  gfx->setCursor(30, 40);
  gfx->print("Saved");
  gfx->setCursor(30, 60);
  gfx->print("Point");
  gfx->setTextColor(YELLOW);
  gfx->setCursor(48, 80);
  gfx->print("#");
  gfx->print(index);
}

// Button coordinates (adjust as needed)
#define BUTTON_Y 120

void draw_button(int x, String label) {
    // Draw button, using fillRect or drawRect (outline).
    // gfx->fillRect(x, BUTTON_Y - BUTTON_HEIGHT / 2, BUTTON_WIDTH, BUTTON_HEIGHT, WHITE);

    gfx->setCursor(x, BUTTON_Y);  // Adjust text position
    gfx->setTextColor(WHITE);
    gfx->setTextSize(1);
    gfx->print(label);  // Label the button
}

void draw_button_indicators() {
    // Draw Button 1
    switch(current_screen)
    {
      case MAX_SCREEN:
        draw_button(0, "OFF");
        break;
      default:
        draw_button(0, "NEXT");
        break;
    }
    // Draw Button 2
    draw_button(48, "BLANK");
    // Draw Button 3
    draw_button(104, "SAVE");
}


#define DOT_RADIUS 5  // Set the radius of the dot

void draw_empty_red_dot() {
    // Draw a red empty dot (outline) in the top-left corner
    gfx->drawCircle(120, 5, DOT_RADIUS, RED);  // (x, y, radius, color)
}

void draw_filled_red_dot() {
    // Draw a red filled dot in the top-left corner
    gfx->fillCircle(120, 5, DOT_RADIUS, RED);  // (x, y, radius, color)
}

#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

void draw_cabs(float absorbance, double abs_corr_factor){
  draw_qty("cABS", absorbance);
  draw_factor(abs_corr_factor);
}

void draw_abs(float absorbance){
  draw_qty("ABS", absorbance);
}

void draw_trans(float transmittance, uint16_t intensity){
  draw_qty("TR", transmittance);
  draw_intensity(intensity);
}
