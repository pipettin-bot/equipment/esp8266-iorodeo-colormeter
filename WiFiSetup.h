
#ifndef WIFISETUP_H
#define WIFISETUP_H

#include <Arduino.h>
#include <ArduinoJson.h>

extern bool is_ap;
void initWiFiDefaults(JsonDocument& wifi_creds);
void initWiFi(JsonDocument& wifi_creds);
void initAP();
void clearWiFiConfig();
void updateWiFiConfig(const String& ssid, const String& password);
String get_ip();
String get_net();

#endif // WIFISETUP_H
