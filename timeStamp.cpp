#include "timeStamp.h"
#include "WiFiSetup.h"
#include <WiFiUdp.h>

#include <NTPClient.h>

// In case you want to have local time timeStamps
const long utcOffsetInSeconds = -3*60*60;  // UTC-3
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

void initTimeService(){
  timeClient.begin();
}

// Returns the timeStamp. If in AP mode or cannot contact NTP server
// returns millis(). Else, returns epoch time
String getTimeStamp(){
  if(is_ap || !timeClient.forceUpdate()){
    return "NA";
  } else{
    return timeClient.getFormattedTime();
  }
}
