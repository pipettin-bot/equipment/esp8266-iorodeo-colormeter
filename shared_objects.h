#ifndef DATA_OBJECTS_H
#define DATA_OBJECTS_H

#include <ArduinoJson.h>

// Json Variable to Hold Sensor Readings
extern JsonDocument readings;
// Json Variable to Hold Sensor Readings used as blank values
extern JsonDocument blanks;
// Json Variable to Hold sensor configuration.
extern JsonDocument sensor_config;
// Json Variable to Hold WiFi credentials
extern JsonDocument wifi_creds;
// Json Variable to calibration data
extern JsonDocument calib_config;

#endif // DATA_OBJECTS_H
