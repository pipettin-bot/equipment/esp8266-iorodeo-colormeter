#ifndef BUTTONS_H
#define BUTTONS_H

#include <OneButton.h>

extern OneButton btn;

void setup_buttons();
void tick_buttons();

#endif // BUTTONS_H
