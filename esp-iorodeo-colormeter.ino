/*

  Firmware code for an ESP-based photometer, built on the Lolin S3 mini pro development board (ESP32-S3).

*/

// Uncomment the following line to enable MQTT connections.
// #define USE_MQTT

#include <Arduino.h>
#include <ArduinoJson.h>

// Include files from this project.
#include "Buttons.h"
#include "WiFiSetup.h"
#include "LittleFSHandler.h"
#include "OLED.h"
#include "json_websocket.h" // JSON-WEBSOCKET processing stuff.
#include "json_rpc.h" // JSON-RPC processing stuff, provides "parse_json_rpc".
#include "Colormeter.h"
#include "sleep.h"
#include "front_led.h"
// Data objects.
#include "shared_objects.h"
#include "timeStamp.h"
// MQTT connection.
#ifdef USE_MQTT
#include "MQTT.h"
#else
#warning "Warning: MQTT connection disabled."
#endif

// Sensor I2C pins.
#ifdef ESP8266
  #define SDA_TSL D2 // D2: default SDA pin for I2C in NodeMCU ESP12-E (ESP8266).
  #define SCL_TSL D1 // D1: default SCL pin for I2C in NodeMCU ESP12-E (ESP8266).
#elif defined(ESP32)
  #define SDA_TSL 12 // GPIO12: JST-SH1.0-4P connector SDA pin for I2C in Lolin S3 Mini Pro (ESP32-S3).
  #define SCL_TSL 11 // GPIO11: JST-SH1.0-4P connector SCL pin for I2C in Lolin S3 Mini Pro (ESP32-S3).
  // https://www.wemos.cc/en/latest/s3/s3_mini_pro.html
  // https://www.wemos.cc/en/latest/_static/files/sch_s3_mini_pro_v1.0.0.pdf
#else
  #error "Unsupported board. Please use ESP8266 or ESP32."
#endif

// Using a second I2C interface for the display is impossible in the ESP8266.
// Even if a second "Wire instance" is created, the last call to "wire.begin"
// will determine which pins are used.
// This is because the ESP8266 only has one I2C bus.

// Timer variables
unsigned long lastTime = 0;
unsigned long read_delay = 1000;

// SETUP //

void setup_serial(){
  // Initialize the serial port.
  Serial.begin(115200);
  delay(1000);  // Give the serial interface time to initialize

  // Wait for the Serial port to connect for up to 10 seconds.
  Serial.println("Serial port Initialized.");
}

bool sensor_connected = false;

void setup(){

  // Initialize the serial port.
  setup_serial();

  #ifdef ESP32
  setup_blink();
  #endif

  // Setup sleep
  setup_sleep();

  // Setup display.
  setup_display();

  // Initialize the file system.
  Serial.println("Initializing LittleFS...");
  initFS();
  // Read state from persistent storage.
  setup_defaults();
  // Initi wifi default credentials.
  initWiFiDefaults(wifi_creds);

  // Draw WiFi startup messge.
  draw_wifi_ip("Connecting...");
  String ssid = wifi_creds["ssid"];
  draw_wifi_ssid(ssid);
  // Start the WiFi.
  Serial.println("Initializing WiFi...");
  initWiFi(wifi_creds);

  // Setup the web server and WiFi connections.
  Serial.println("Initializing Web server...");
  setup_webserver();

  // Setup MQTT connection.
  #ifdef USE_MQTT
  setup_mqtt();
  #endif

  // Setup NTP connection.
  initTimeService();

  // Setup the sensor.
  Serial.println("Initializing Sensor...");
  // Set I2C pins for the sensor.
  Wire.begin(SDA_TSL, SCL_TSL);
  // Connect to the sensor.
  sensor_connected = setup_sensor();

  // Setup buttons.
  setup_buttons();

  Serial.println("Setup complete. Happy metering!");
  clear_display();
}


// LOOP //

void lazy_loop() {

  // Sensor.
  if (sensor_connected){
    // Produce new readings from the sensor and recompute measurements.
    loop_sensor();

    // Get the latest readings.
    String sensorReadings = serialize_state();

    // Send the readings to all connected websocket clients.
    notifyClients(sensorReadings);

  } else {
    // Display error message.
    String msg = "Sensor setup failed.";
    Serial.println(msg);
    // draw_error(msg);
    // Try to reconnect.
    sensor_connected = setup_sensor();
  }
  
  // MQTT connection.
  #ifdef USE_MQTT
  loop_mqtt();
  #endif
  
  // Update the display
  update_display();
  
  // Websocket.
  cleanupClients();
}

int last_screen = 1;
void loop(){

  if ((millis() - lastTime) > read_delay) {
    // Update last time.
    lastTime = millis();
    // Read from the sensor and draw the main screen.
    lazy_loop();
  } else if (last_screen != current_screen){
    // Update the display if the screen has changed.
    update_display();
  }
  
  // Save screen state, before looking for button clicks.
  last_screen = current_screen;

  // Look for clicks on all buttons.
  tick_buttons();

  // #ifdef ESP32
  // // Front LED.
  // tick_front_led();
  // #endif

  // Parse incoming messages and run other JSON-RPC logic.
  parse_json_rpc();
}

void update_display(){
  // Only do the update if the display is on.
  if(display_state){
    // Get measurements to print into display
    float absorbance=readings["vis_abs"].as<float>();
    float transmittance=readings["vis_trans"].as<float>();
    // Get absorbance correction factor.
    double abs_corr_factor = calib_config["factor"].as<double>();
    // Get visible intensity.
    uint16_t intensity = readings["vis_lum"].as<uint16_t>();
    // Get network information
    String ip_address = get_ip();
    String ssid = get_net();
    clear_display();
    
    // Draw the measurement that corresponds to the current "screen".
    // See "current_screen" and "display_next" in "OLED.cpp".
    switch(current_screen)
    {
      case 1:
        // Factor-corrected absorbance. 
        draw_cabs(absorbance, abs_corr_factor);
        break;
      case 2:
        // Raw absorbance.
        draw_abs(absorbance);
        break;
      case 3:
        // Raw transmittance.
        draw_trans(transmittance, intensity);
        break;
      case 4:
        // Info screen.
        showLogo_regosh();
        break;
    }
    // Draw WiFi connection info.
    draw_wifi_ip(ip_address);
    draw_wifi_ssid(ssid);

    // Draw buttons.
    draw_button_indicators();

    // Add remaining elements unless its the info screen.
    if(current_screen != 4){
      if (sensor_connected){
        draw_filled_red_dot();
      } else {
        draw_empty_red_dot();
      }
      // Draw the next measurement's id.
      draw_index(savedCount+1);
    }

    // Update the display.
    draw_display();
  }
}
