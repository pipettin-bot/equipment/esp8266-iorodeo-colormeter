#include <ArduinoJson.h>
#include "shared_objects.h"

// Json Variable to Hold Sensor Readings
JsonDocument readings;
// Json Variable to Hold Sensor Readings used as blank values
JsonDocument blanks;
// Json Variable to Hold sensor configuration.
JsonDocument sensor_config;
// Json Variable to Hold WiFi credentials
JsonDocument wifi_creds;
// Json Variable to calibration data
JsonDocument calib_config;
