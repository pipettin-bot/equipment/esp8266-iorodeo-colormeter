
#ifndef THERMAL_H
#define THERMAL_H

void fit_thermal_calibration();
float thermal_correction(float rawIntensity);

#endif // THERMAL_H
